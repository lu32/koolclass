<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Auth;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseTransactions;

    protected function signIn($user = null, $type = User::TEACHER_TYPE) : User
    {
        $user = $user?: factory(\App\User::class)->create(['type' => $type]);
        Auth::login($user);
        $this->actingAs($user);

        return $user;
    }

    protected function studentSignIn($user = null)
    {
        return $this->signIn($user, User::STUDENT_TYPE);
    }

    protected function teacherSignIn($user = null)
    {
        return $this->signIn($user, User::TEACHER_TYPE);
    }

    protected function adminSignIn($user = null)
    {
        return $this->signIn($user, User::ADMIN_TYPE);
    }

    protected function requiresAuth($route)
    {
        $this->getJson($route)->assertStatus(401);
        $this->get($route)->assertRedirect(route('login'));
    }
}
