<?php

namespace Tests\Feature\Courses;

use App\Course;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateCourseTest extends TestCase
{
    /** @test */
    public function a_user_can_update_his_courses()
    {
        $this->withoutExceptionHandling();
        $profe = $this->signIn();
        $course = create(Course::class, ['creator_id' => $profe->id, 'name' => 'Math', 'institution_id' => $profe->institution_id]);
        $this->patch(route('update-course', [$course->id]), ['name' => 'Spanish'])
            ->assertStatus(302)
            ->assertSessionHas('success', __('copies.update_course.success_message'));
        $this->assertEquals('Spanish', $course->fresh()->name);
    }

    /** @test */
    public function a_guest_user_cant_update_courses()
    {
        $course = create(Course::class, ['name' => 'Math']);
        $this->patch(route('update-course', [$course->id]), ['name' => 'Spanish'])
            ->assertStatus(302)
            ->assertRedirect(route('login'));
        $this->patchJson(route('update-course', [$course->id]), ['name' => 'Spanish'])
            ->assertStatus(401);
        $this->assertEquals('Math', $course->fresh()->name);
    }

    /** @test */
    public function a_teacher_cant_update_anothers_teacher_course()
    {
        $cristoReyTeacher = create(User::class);
        $course = create(Course::class, ['creator_id' => $cristoReyTeacher->id, 'name' => 'Math']);
        $buenConsejoTeacher = $this->signIn();
        $this->patchJson(route('update-course', [$course->id]), ['name' => 'Spanish'])
            ->assertStatus(403);
        $this->assertEquals('Math', $course->fresh()->name);
    }

}
