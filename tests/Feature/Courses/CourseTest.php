<?php

namespace Tests\Feature\Courses;

use App\Course;
use App\Folder;
use App\Group;
use App\Message;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CourseTest extends TestCase
{
   /** @test */
   public function a_course_has_a_creator()
   {
       $profeDamelys = create(User::class);
       $course = create(Course::class, ['creator_id' => $profeDamelys->id]);
       $this->assertInstanceOf(User::class, $course->creator);
   }

   /** @test */
   public function a_course_has_a_folder()
   {
       $course = create(Course::class);
       $courseFolder = create(Folder::class, ['course_id' => $course->id]);
       $this->assertInstanceOf(Folder::class, $course->mainFolder);
   }

   /** @test */
   public function a_course_has_many_groups()
   {
       $group = create(Group::class);
       $course = create(Course::class);
       $course->groups()->save($group);
       $this->assertInstanceOf(Group::class, $course->groups->first());
   }


   /** @test */
   public function a_course_have_many_messages()
   {
       $course = create(Course::class);
       $message = make(Message::class);
       $course->messages()->save($message);
       $this->assertInstanceOf(Message::class, $course->messages->first());
   }



}
