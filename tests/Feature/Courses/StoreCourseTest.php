<?php

namespace Tests\Feature\Courses;

use App\Course;
use App\PeriodsCollection;
use Tests\TestCase;

class StoreCourseTest extends TestCase
{
    /** @test */
    public function a_course_can_be_stored_only_if_logged_in()
    {
        $this->postJson(route('store-course'), ['name' => 'Math'])->assertStatus(401);
    }

    /** @test */
    public function a_user_can_store_a_course()
    {
        $this->withoutExceptionHandling();
        $profe = $this->signIn();
        $course = Course::first();
        $this->assertNull($course);
        $response = $this->post(route('store-course'), [
            'name' => 'Math',
            'year' => now()->year,
            'period' => PeriodsCollection::PERIOD_ALL_YEAR
        ])
            ->assertRedirect()
            ->assertSessionHas(['success' => __('copies.store_course.success_message')]);
        $course = Course::first();
        $this->assertNotNull($course);
        $this->assertEquals($profe->id, $course->creator->id);
        $this->assertEquals($profe->institution_id, $course->institution_id);
        $response->assertRedirect(route('show-course', [$course->id]));
    }

    /** @test */
    public function students_cant_create_courses()
    {
        $student = $this->studentSignIn();
        $response = $this->postJson(route('store-course'), [
            'name' => 'Math',
            'year' => now()->year,
            'period' => PeriodsCollection::PERIOD_ALL_YEAR
        ])->assertStatus(403);
    }



}
