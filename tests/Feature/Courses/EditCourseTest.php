<?php

namespace Tests\Feature\Courses;

use App\Course;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EditCourseTest extends TestCase
{
   /** @test */
   public function a_user_can_see_the_edit_form_of_his_course()
   {
       $this->withoutExceptionHandling();
       $profe = $this->signIn();
       $course = create(Course::class, ['creator_id' => $profe->id, 'institution_id' => $profe->institution_id]);
       $this->get(route('edit-course', [$course->id]))
           ->assertStatus(200)
           ->assertSee(route('show-course', [$course->id]))
           ->assertSee(route('update-course', [$course->id]));
   }

   /** @test */
   public function a_guest_cant_see_the_edit_page()
   {
       $course = create(Course::class);
       $this->getJson(route('edit-course', [$course->id]))
           ->assertStatus(401);
   }

    /** @test */
    public function a_teacher_cant_see_anothers_teacher_edit_course_page()
    {
        $buenConsejoTeacher = $this->signIn();
        $cristoReyTeacher = create(User::class);
        $course = create(Course::class, ['creator_id' => $cristoReyTeacher->id]);
        $this->getJson(route('edit-course', [$course->id]))
            ->assertStatus(403);
    }


}
