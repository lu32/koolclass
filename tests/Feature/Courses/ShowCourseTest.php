<?php

namespace Tests\Feature\Courses;

use App\Course;
use App\Institution;
use App\User;
use App\Folder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowCourseTest extends TestCase
{
    /** @test */
    public function a_logged_id_user_can_see_a_course()
    {
        $this->withoutExceptionHandling();
        $profe = $this->signIn();
        $course = create(Course::class, ['creator_id' => $profe->id, 'institution_id' => $profe->institution_id]);
        create(Folder::class, ['course_id' => $course->id]);
        $this->get(route('show-course', [$course->id]))
            ->assertStatus(200)
            ->assertSee(route('edit-course', [$course->id]))
            ->assertSeeText($course->name);
    }

    /** @test */
    public function a_teacher_can_see_anothers_teacher_course()
    {
        $buenConsejoTeacher = $this->signIn();
        $cristoReyTeacher = create(User::class);
        $course = create(Course::class, [
            'creator_id' => $cristoReyTeacher->id,
            'institution_id' => $cristoReyTeacher->institution_id]);
        $this->getJson(route('show-course', [$course->id]))
            ->assertStatus(403);
    }

    /** @test */
    public function users_of_the_same_institution_can_see_the_course_page()
    {
        $cristoRey = create(Institution::class);
        $gloriaBalagueraTeacher = create(User::class, ['institution_id' => $cristoRey->id, 'type' => User::TEACHER_TYPE]);
        $ancizarCruxTeacher = create(User::class, ['institution_id' => $cristoRey->id, 'type' => User::TEACHER_TYPE]);
        $divierPerez = create(User::class, ['institution_id' => $cristoRey->id, 'type' => User::STUDENT_TYPE]);
        $math = create(Course::class, [
            'creator_id' => $gloriaBalagueraTeacher->id,
            'institution_id' => $cristoRey->id,
            'name' => 'Matemáticas',
        ]);
        $users = [$divierPerez, $ancizarCruxTeacher, $gloriaBalagueraTeacher];
        foreach ($users as $user){
            $this->signIn($user);
            $this->getJson(route('show-course', [$math->id]))->assertStatus(200);
        }
    }

    /** @test */
    public function a_guest_cant_see_the_course_show_page()
    {
        $course = create(Course::class);
        $this->getJson(route('show-course', [$course->id]))
            ->assertStatus(401);
    }

    /** @test */
    public function a_success_message_is_shown()
    {
        $this->withoutExceptionHandling();
        $profe = $this->signIn();
        $course = create(Course::class, ['creator_id' => $profe->id, 'institution_id' => $profe->institution_id]);
        create(Folder::class, ['course_id' => $course->id]);
        $this->session(['success' => 'Some message'])->get(route('show-course', [$course->id]))
            ->assertStatus(200)
            ->assertSeeText('Some message');
    }

}
