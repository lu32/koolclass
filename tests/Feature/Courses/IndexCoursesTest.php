<?php

namespace Tests\Feature\Courses;

use App\Course;
use App\Http\Livewire\CoursesIndex;
use App\Institution;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Livewire;

class IndexCoursesTest extends TestCase
{
    /** @test */
    public function a_teacher_can_see_his_courses_index()
    {
        $missMolina = $this->signIn();
        $courses = create(Course::class, [
                       'creator_id' => $missMolina->id,
                       'institution_id' => $missMolina->institution_id], 50);
        $this->get(route('index-courses'))
            ->assertStatus(200)
            ->assertSeeText(__('copies.index_courses.card_title'))
            ->assertSeeTextInOrder($courses->pluck('name')->all());
    }

    /** @test */
    public function users_can_see_the_courses_courses()
    {
        $this->withoutExceptionHandling();
        $inscrey = create(Institution::class);
        $missMolina = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);
        $divierPerez = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
        $this->signIn($missMolina);
        $sirCordoba = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);
        $missMolinaCourses = create(Course::class, ['creator_id' => $missMolina->id, 'institution_id' => $inscrey->id], 5);
        $sirCordobaCourses = create(Course::class, ['creator_id' => $sirCordoba->id, 'institution_id' => $inscrey->id], 5);
        foreach ($sirCordobaCourses as $sirCordobaCourse) {
            $this->get(route('index-courses'))
                ->assertStatus(200)
                ->assertSeeText($sirCordobaCourse->name);
        }
        foreach ($missMolinaCourses as $missMolinaCourse) {
            $this->get(route('index-courses'))
                ->assertStatus(200)
                ->assertSeeText($missMolinaCourse->name);
        }
        $this->signIn($divierPerez);
        foreach ($sirCordobaCourses as $sirCordobaCourse) {
            $this->get(route('index-courses'))
                ->assertStatus(200)
                ->assertSeeText($sirCordobaCourse->name);
        }
        foreach ($missMolinaCourses as $missMolinaCourse) {
            $this->get(route('index-courses'))
                ->assertStatus(200)
                ->assertSeeText($missMolinaCourse->name);
        }

    }

    /** @test */
    public function a_guest_cant_see_the_courses_index()
    {
        $courses = create(Course::class, [], 50);
        $this->getJson(route('index-courses'))
            ->assertStatus(401);
        $this->get(route('index-courses'))
            ->assertRedirect(route('login'));
    }

    /** @test */
    public function users_from_another_institution_can_see_the_courses_index()
    {
        $this->withoutExceptionHandling();
        $inscrey = create(Institution::class);
        $buenConsejo = create(Institution::class);
        $missRaquel = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $buenConsejo->id]);
        $mariaMontes = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $buenConsejo->id]);
        $missMolina = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);
        $divierPerez = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
        $this->signIn($missRaquel);
        $sirCordoba = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);
        $missMolinaCourses = create(Course::class, ['creator_id' => $missMolina->id, 'institution_id' => $inscrey->id], 5);
        $sirCordobaCourses = create(Course::class, ['creator_id' => $sirCordoba->id, 'institution_id' => $inscrey->id], 5);

        foreach ($sirCordobaCourses as $sirCordobaCourse) {
            $this->get(route('index-courses'))
                ->assertStatus(200)
                ->assertDontSeeText($sirCordobaCourse->name);
        }
        foreach ($missMolinaCourses as $missMolinaCourse) {
            $this->get(route('index-courses'))
                ->assertStatus(200)
                ->assertDontSeeText($missMolinaCourse->name);
        }
        $this->signIn($mariaMontes);
        foreach ($sirCordobaCourses as $sirCordobaCourse) {
            $this->get(route('index-courses'))
                ->assertStatus(200)
                ->assertDontSeeText($sirCordobaCourse->name);
        }
        foreach ($missMolinaCourses as $missMolinaCourse) {
            $this->get(route('index-courses'))
                ->assertStatus(200)
                ->assertDontSeeText($missMolinaCourse->name);
        }
    }

    /** @test */
    public function students_cant_see_the_create_course_button()
    {
        $di = $this->studentSignIn();
        Livewire::test(CoursesIndex::class)->assertDontSee('create-course-button');
    }

    /** @test */
    public function teachers_can_see_the_create_course_button()
    {
        $julio = $this->teacherSignIn();
        Livewire::test(CoursesIndex::class)->assertSee('create-course-button');
    }





}
