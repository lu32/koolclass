<?php

namespace Tests\Feature\Courses;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateCourseTest extends TestCase
{
    /** @test */
    public function a_user_can_see_the_create_course_view()
    {
        $missMolina = $this->signIn();
        $this->get(route('create-course'))
            ->assertStatus(200)
            ->assertSee(route('store-course'))
            ->assertSee(__('copies.create_course.card_title'));
    }

    /** @test */
    public function a_guest_user_cant_see_the_create_course_view()
    {
        $this->getJson(route('create-course'))
            ->assertStatus(401);
        $this->get(route('create-course'))
            ->assertRedirect(route('login'));

    }
}
