<?php

namespace Tests\Feature;

use App\Course;
use App\Group;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function a_user_has_many_courses()
    {
        $missMolina = create(User::class);
        $course = create(Course::class, ['creator_id' => $missMolina->id]);
        $this->assertInstanceOf(Course::class, $missMolina->courses->first());
    }

    /** @test */
    public function it_creates_a_options_array()
    {
        $options = User::dropdownOptions();
        $this->assertEquals(User::STUDENT_TYPE, $options[1]->id);
        $this->assertEquals(__("copies.user_types." . User::STUDENT_TYPE), $options[1]->name);
    }

    /** @test */
    public function a_user_can_create_many_groups()
    {
        $missMolina = create(User::class);
        $missMolinaGroup = create(Group::class, [
            'institution_id' => $missMolina->institution_id,
            'creator_id' => $missMolina->id
        ]);
        $this->assertInstanceOf(Group::class, $missMolina->createdGroups->first());
    }


}
