<?php

namespace Tests\Feature\Institutions;

use App\Institution;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InstitutionTest extends TestCase
{
    /** @test */
    public function a_insitution_has_many_teachers()
    {
        $inscrey = create(Institution::class);
        $teachers = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id], 2);
        $students = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id], 2);
        $this->assertCount(2, $inscrey->teachers);
        $this->assertEquals($teachers[0]->id, $inscrey->teachers[0]->id);
        $this->assertEquals($teachers[1]->id, $inscrey->teachers[1]->id);
    }

    /** @test */
    public function institution_has_many_users()
    {
        $inscrey = create(Institution::class);
        $users = create(User::class, ['institution_id' => $inscrey->id], 2);
        $this->assertCount(2, $inscrey->users);
        $this->assertInstanceOf(User::class, $inscrey->users->first());
    }


}
