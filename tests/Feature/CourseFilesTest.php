<?php

namespace Tests\Feature;

use App\Course;
use App\Folder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CourseFilesTest extends TestCase
{
   /** @test */
   public function a_user_can_see_the_files_view()
   {
       $missMolina = $this->signIn();
       $course = create(Course::class, ['creator_id' => $missMolina->id, 'institution_id' => $missMolina->institution_id]);
       $folder = new Folder;
       $folder -> name = 'Test folder';
       $folder->course_id = $course->id;
       $folder->save();

       $this->get(route('course-files', [$course->id, $course->mainFolder]))->assertStatus(200);
   }

}
