<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchUsersTest extends TestCase
{
    /** @test */
    public function searching_users_requires_auth()
    {
        $response = $this->getJson(route('search-users'));
        $response->assertStatus(401);
    }

    /** @test */
    public function the_search_works()
    {
        $user = $this->teacherSignIn();
        $nemo = create(User::class, [
            'institution_id' => $user->institution_id,
            'first_name' => 'Nemo',
            'last_name' => 'Clown',
            'local_id' => '333',
        ]);

        $jose = create(User::class, [
            'institution_id' => $user->institution_id,
            'first_name' => 'jose',
            'last_name' => 'moreno',
            'local_id' => '444',
        ]);

        $this->getJson(route('search-users', ['first_name' => 'Nemo']))
            ->assertJson([
                'data' => [
                    [
                        'first_name' => 'Nemo',
                        'last_name' => $last_name = 'Clown',
                        'local_id' => '333',
                    ]
                ]
            ])->assertJsonCount(1, 'data');

        $this->getJson(route('search-users', ['last_name' => \Str::lower($last_name)]))
            ->assertJson([
                'data' => [
                    [
                        'first_name' => 'Nemo',
                        'last_name' => 'Clown',
                        'local_id' => '333',
                    ]
                ]
            ])->assertJsonCount(1, 'data');

        $this->getJson(route('search-users', ['local_id' => 333]))
            ->assertJson([
                'data' => [
                    [
                        'first_name' => 'Nemo',
                        'last_name' => 'Clown',
                        'local_id' => '333',
                    ]
                ]
            ])->assertJsonCount(1, 'data');

        $this->getJson(route('search-users', ['local_id' => 33, 'first_name' => 'ne', 'last_name' => 'clo']))
            ->assertJson([
                'data' => [
                    [
                        'first_name' => 'Nemo',
                        'last_name' => 'Clown',
                        'local_id' => '333',
                    ]
                ]
            ])->assertJsonCount(1, 'data');


    }

    /** @test */
    public function the_search_only_works_for_users_in_the_same_institution()
    {
        $user = $this->teacherSignIn();
        $nemo = create(User::class, [
            'institution_id' => $user->institution_id,
            'first_name' => 'Nemo',
            'last_name' => 'Clown',
            'local_id' => '333',
        ]);

        $nemoRosario = create(User::class, [
            'first_name' => 'Nemo',
            'last_name' => 'Clown',
            'local_id' => '333',
        ]);

        $this->getJson(route('search-users', ['first_name' => 'Nemo']))
            ->assertJson([
                'data' => [
                    [
                        'id' => $nemo->id,
                    ]
                ]
            ])->assertJsonCount(1, 'data');

    }


    /** @test */
    public function searchs_by_type()
    {
        $user = $this->teacherSignIn();
        $nemo = create(User::class, [
            'institution_id' => $user->institution_id,
            'first_name' => 'Nemo',
            'last_name' => 'Clown',
            'local_id' => '333',
            'type' => User::STUDENT_TYPE,
        ]);

        $nemoTeacher = create(User::class, [
            'institution_id' => $user->institution_id,
            'first_name' => 'Nemo',
            'last_name' => 'Clown',
            'local_id' => '444',
            'type' => User::TEACHER_TYPE,
        ]);

        $this->getJson(route('search-users', ['first_name' => 'Nemo', 'type' => User::STUDENT_TYPE]))
            ->assertJson([
                'data' => [
                    [
                        'id' => $nemo->id,
                    ]
                ]
            ])->assertJsonCount(1, 'data');

    }


}
