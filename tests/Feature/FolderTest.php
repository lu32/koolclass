<?php

namespace Tests\Feature;

use App\Folder;
use App\Upload;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FolderTest extends TestCase
{
    /** @test */
    public function a_folder_has_many_uploads()
    {
        $rootDir = create(Folder::class);
        $upload = create(Upload::class, ['folder_id' => $rootDir->id]);
        $this->assertInstanceOf(Upload::class, $rootDir->uploads->first());
    }

}
