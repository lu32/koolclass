<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomePageTest extends TestCase
{
    /** @test */
    public function a_guest_user_can_see_the_home_page()
    {
        $this->get('/')->assertStatus(200)->assertSee(__('copies.home.welcome_title.first_part'));
    }

    /** @test */
    public function a_logged_in_user_is_redirected_from_the_home_to_the_dashboard()
    {
        $this->signIn();
        $this->get('/')->assertRedirect(route('dashboard'));
    }

}
