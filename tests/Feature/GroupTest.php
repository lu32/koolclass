<?php

namespace Tests\Feature;

use App\Course;
use App\Institution;
use App\Group;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GroupTest extends TestCase
{
    /** @test */
    public function a_group_has_a_creator()
    {
        $group = create(Group::class);
        $this->assertInstanceOf(User::class, $group->creator);
    }

    /** @test */
    public function a_group_belongs_to_an_institution()
    {
        $group = create(Group::class);
        $this->assertInstanceOf(Institution::class, $group->institution);
    }

    /** @test */
    public function a_group_has_many_courses()
    {
        $group = create(Group::class);
        $course = create(Course::class);
        $group->courses()->save($course);
        $this->assertInstanceOf(Course::class, $group->courses->first());
    }

    /** @test */
    public function a_group_has_many_students()
    {
        $group = create(Group::class);
        $student = create(User::class, ['type' => 'student']);
        $group->students()->save($student);
        $this->assertInstanceOf(User::class, $group->students->first());
    }

    /** @test */
    public function a_group_has_only_students()
    {
        $insSanFrancisco = create(Institution::class, ['name' => 'I.E.D.SAN FRANCISCO JAVIER']);
        $group = create(Group::class);
        $student = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $insSanFrancisco->id,]);
        $teacher = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $insSanFrancisco->id,]);
        $admin = create(User::class, ['type' => User::ADMIN_TYPE, 'institution_id' => $insSanFrancisco->id,]);
        $group->students()->save($teacher);
        $group->students()->save($student);
        $group->students()->save($admin);
        $this->assertEquals($student->id, $group->students->first()->id);
        $this->assertCount(1, $group->students);
    }


}
