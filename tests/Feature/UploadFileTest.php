<?php

namespace Tests\Feature;

use App\Folder;
use App\Course;
use App\Institution;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Storage;
use Str;

class UploadFileTest extends TestCase
{
    /** @test */
    public function a_user_cant_upload_files_to_another_user_course_folder()
    {
        // $this->withoutExceptionHandling();
        $institution = create(Institution::class);
        $missMolina = create(User::class, ['institution_id' => $institution->id]);
        $mrCordoba = create(User::class, ['institution_id' => $institution->id]);
        $missMolinaCourse = create(Course::class, ['creator_id' => $missMolina->id]);
        $mrCordobaCourse = create(Course::class, ['creator_id' => $mrCordoba->id]);
        create(Folder::class, ['course_id' => $missMolinaCourse->id]);
        create(Folder::class, ['course_id' => $mrCordobaCourse->id]);
        $this->signIn($mrCordoba);
        $this->postJson(route('store-file'), [
            'key' => 'someS3FileKey',
            'folder_id' => $missMolinaCourse->mainFolder->id,
            'filename' => 'document.pdf',
        ])->assertStatus(403);
    }

    /** @test */
    public function the_signed_url_is_protected()
    {
        $this->postJson(route('signed-url'))->assertStatus(401);
    }

    /** @test */
    public function a_user_can_upload_a_file_to_a_folder()
    {
        $this->withoutExceptionHandling();
        Storage::fake('s3');
        $uuid = Str::uuid();
        $key = 'tmp/' . $uuid;
        Storage::disk('s3')->put($key, 'file content');
        Storage::disk('s3')->assertExists($key);
        $missMolina = create(User::class);
        $missMolinaCourse = create(Course::class, ['creator_id' => $missMolina->id]);
        $missMolinaCourseMainFolder = create(Folder::class, ['course_id' => $missMolinaCourse->id]);
        $this->signIn($missMolina);
        $this->postJson(route('store-file'), [
            'key' => $key,
            'folder_id' => $missMolinaCourse->mainFolder->id,
            'filename' => 'document.pdf',
        ])->assertStatus(201)->assertJson([
            'status' => 'ok'
        ]);
        $this->assertCount(1, Storage::disk('s3')->allFiles('uploads'));
        $this->assertCount(1, $missMolinaCourse->mainFolder->uploads);
    }


}
