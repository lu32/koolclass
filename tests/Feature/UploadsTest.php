<?php

namespace Tests\Feature;

use App\Folder;
use App\Upload;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UploadsTest extends TestCase
{
    /** @test */
    public function a_upload_can_belongs_to_a_folder()
    {
        $folder = create(Folder::class);
        $upload = create(Upload::class, ['folder_id' => $folder->id]);
        $this->assertInstanceOf(Folder::class, $upload->folder);
    }

}
