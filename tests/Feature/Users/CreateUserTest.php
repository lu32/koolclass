<?php

namespace Tests\Feature\Users;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    /** @test */
    public function teachers_and_admins_can_see_the_create_user_view_students_no()
    {
        $this->teacherSignIn();
        $this->get(route('create-user'))->assertStatus(200);
        $this->adminSignIn();
        $this->get(route('create-user'))->assertStatus(200);
        $this->studentSignIn();
        $this->get(route('create-user'))->assertStatus(403);
    }

}
