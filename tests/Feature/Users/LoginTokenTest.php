<?php

namespace Tests\Feature\Users;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTokenTest extends TestCase
{
    /** @test */
    public function login_token_is_for_guests_only()
    {
        $this->studentSignIn();
        $this->get(route('login-token', ['token']))->assertRedirect(route('home'));
    }

    /** @test */
    public function a_user_can_visit_the_login_token_page()
    {
        $user = create(User::class, ['login_token' => \Str::random(20)]);
        $this->get(route('login-token', [$user->login_token]))
            ->assertStatus(200)->assertSee($user->first_name);
    }

    /** @test */
    public function it_needs_a_valid_login_token_show_error_message()
    {
        $this->get(route('login-token', [\Str::random(20)]))
            ->assertStatus(200)->assertSee(__('copies.login_token.invalid_token_message'));
    }

    /** @test */
    public function a_user_can_update_his_password_and_login()
    {
        $user = create(User::class, ['login_token' => \Str::random(20), 'password' => null]);
        $this->post(route('login-token', [$user->login_token]), ['password' => '123456', 'password_confirmation' => '123456'])
            ->assertRedirect(route('dashboard', ['message' => 'si']));
        $this->assertNotNull(auth()->user());
        $this->assertEquals(auth()->user()->id, $user->id);
        $this->assertNotNull($user->fresh()->password);
    }

    /** @test */
    public function the_new_password_is_required()
    {
        $user = create(User::class, ['login_token' => \Str::random(20), 'password' => null]);
        $this->post(route('login-token', [$user->login_token]))
            ->assertSessionHasErrors('password');
    }

    /** @test */
    public function the_password_min_length_is_6()
    {
        $user = create(User::class, ['login_token' => \Str::random(20), 'password' => null]);
        $this->post(route('login-token', [$user->login_token, 'password' => '12345', 'password_confirmation' => '12345']))
            ->assertSessionHasErrors('password');
        $this->post(route('login-token', [$user->login_token, 'password' => '123456', 'password_confirmation' => '123456']))
            ->assertSessionHasNoErrors('password');
    }

    /** @test */
    public function the_password_is_confirmed()
    {
        $user = create(User::class, ['login_token' => \Str::random(20), 'password' => null]);
        $this->post(route('login-token', [$user->login_token, 'password' => '123456', 'password_confirmation' => '1234567']))
            ->assertSessionHasErrors('password');
        $this->post(route('login-token', [$user->login_token, 'password' => '123456', 'password_confirmation' => '123456']))
            ->assertSessionHasNoErrors();
    }


}
