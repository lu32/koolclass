<?php

namespace Tests\Feature\Users;

use App\Institution;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowUserTest extends TestCase
{
   /** @test */
   public function show_a_user_requires_authentication()
   {
       $this->requiresAuth(route('show-user', [1]));
   }

   /** @test */
   public function a_user_can_see_a_user_page()
   {
       $inscrey = create(Institution::class, ['name' => 'Instituto Cristo Rey']);
       $elRosario = create(Institution::class, ['name' => 'Colegio el Rosario']);

       $andresFelipe = create(User::class, ['institution_id' => $elRosario]);

       $senoBlanca = create(User::class, ['type' => User::ADMIN_TYPE, 'institution_id' => $inscrey->id]);
       $divierPerez = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
       $profeManuel = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);

       $inscreyUsers = [$senoBlanca, $divierPerez, $profeManuel];

       foreach ($inscreyUsers as $currentUser) {
           $this->signIn($currentUser);
           foreach ($inscreyUsers as $inscreyUser) {
               $this->get(route('show-user', [$inscreyUser->id]))
                   ->assertStatus(200)
                   ->assertSee($inscreyUser->first_name)
                   ->assertSee($inscreyUser->last_name)
                   ->assertSee($inscreyUser->local_id)
                   ->assertSee($inscreyUser->email)
                   ->assertSee($inscreyUser->phone)
                   ->assertSee(__('copies.user_types.' . $inscreyUser->type));
           }

           $this->get(route('show-user', [$andresFelipe->id]))
               ->assertStatus(403);
       }
   }

   /** @test */
   public function success_message_is_shown()
   {
       $inscrey = create(Institution::class, ['name' => 'Instituto Cristo Rey']);
       $divierPerez = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
       $this->signIn($divierPerez);
       $this->withSession(['success' => $success = 'some success message'])->get(route('show-user', [$divierPerez->id]))
           ->assertStatus(200)->assertSee($success);
   }

   /** @test */
    public function the_login_token_can_only_can_not_be_seen_by_students()
    {
        $inscrey = create(Institution::class, ['name' => 'Instituto Cristo Rey']);
        $senoBlanca = create(User::class, ['type' => User::ADMIN_TYPE, 'institution_id' => $inscrey->id]);
        $cristianBetter = create(User::class, ['last_name' => 'better', 'type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id, 'login_token' => \Str::random(20)]);
        $divierPerez = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
        $profeManuel = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);
        $luzma = create(User::class, ['type' => User::COORDINATOR_TYPE, 'institution_id' => $inscrey->id]);

        $inscreyUsers = [$senoBlanca, $divierPerez, $profeManuel, $luzma];

        foreach ($inscreyUsers as $currentUser) {
           $this->signIn($currentUser);
           if($currentUser->type == User::STUDENT_TYPE) {
               $this->get(route('show-user', [$cristianBetter->id]))
                   ->assertStatus(200)
                   ->assertDontSee($cristianBetter->login_token);
           } else {
               $this->get(route('show-user', [$cristianBetter->id]))
                   ->assertStatus(200)
                   ->assertSee($cristianBetter->login_token);
           }
        }

    }


}
