<?php

namespace Tests\Feature\Users;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Institution;

class UpdateUserTest extends TestCase
{
   /** @test */
   public function updating_a_user_redirects_to_the_show_user_with_a_message()
   {
       $inscrey = create(Institution::class, ['name' => 'Instituto Cristo Rey']);
       $senoBlanca = create(User::class, ['type' => User::ADMIN_TYPE, 'institution_id' => $inscrey->id]);
       $vilma = create(User::class, [
            'first_name' => 'Vilma',
             'last_name' => 'Zarate',
              'local_id' => '1120',
                 'email' => 'vilmaz@koolclass.com',
                 'phone' => '111',
                  'type' => User::STUDENT_TYPE,
        'institution_id' => $inscrey->id,
       ]);

       $this->signIn($senoBlanca);
       $this->patch(route('update-user', [$vilma->id]))
           ->assertRedirect(route('show-user', [$vilma->id]))
           ->assertSessionHas('success');
   }

   protected function createVilma($inscrey)
   {
       return create(User::class, [
           'first_name' => 'Vilma',
           'last_name' => 'Zarate',
           'local_id' => '1120',
           'email' => 'vilmaz@koolclass.com',
           'phone' => '111',
           'type' => User::STUDENT_TYPE,
           'institution_id' => $inscrey->id,
       ]);
   }

   /** @test */
   public function admins_can_update_users_field_by_field()
   {
       $inscrey = create(Institution::class, ['name' => 'Instituto Cristo Rey']);
       $senoBlanca = create(User::class, ['type' => User::ADMIN_TYPE, 'institution_id' => $inscrey->id]);
       $this->signIn($senoBlanca);
       $vilma = $this->createVilma($inscrey);
       $fields = [
           'first_name' => 'Vilma Marcela',
           'last_name' => 'Zarate Daza',
           'local_id' => '1120741',
           'email' => 'vzarate@koolclass.com',
           'phone' => '222',
           'type' => User::TEACHER_TYPE,
       ];
       foreach ($fields as $field => $value) {
           $this->patch(route('update-user', [$vilma->id]), [$field => $value])
               ->assertRedirect(route('show-user', [$vilma->id]))
               ->assertSessionHas('success', __('copies.update_user.success_message'));
           if($field == 'type') {
               $this->assertEquals(User::TEACHER_TYPE, $vilma->fresh()->$field);
           } else {
               $this->assertEquals($value, $vilma->fresh()->$field);
           }


       }
   }

   /** @test */
   public function students_cant_update_other_users()
   {
       $inscrey = create(Institution::class, ['name' => 'Instituto Cristo Rey']);
       $carolina = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
       $this->signIn($carolina);
       $vilma = $this->createVilma($inscrey);
       $fields = [
           'first_name' => 'Vilma Marcela',
           'last_name' => 'Zarate Daza',
           'local_id' => '1120741',
           'email' => 'vzarate@koolclass.com',
           'phone' => '222',
           'type' => User::TEACHER_TYPE,
       ];
       foreach ($fields as $field => $value) {
           $this->patch(route('update-user', [$vilma->id]), [$field => $value])
               ->assertRedirect(route('show-user', [$vilma->id]))
               ->assertSessionHas('error', __('copies.update_user.permission_error'));
           $this->assertEquals($vilma->$field, $vilma->fresh()->$field);
       }
   }

   /** @test */
   public function validates_unique_local_id()
   {
       $this->withExceptionHandling();
       $inscrey = create(Institution::class, ['name' => 'Instituto Cristo Rey']);
       $carolina = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id, 'local_id' => 111]);
       $katherine = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id, 'local_id' => 222]);
       $luzma = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id, 'local_id' => 333]);
       $this->signIn($luzma);
       $this->patch(route('update-user', $carolina->id), ['local_id' => 222])
           ->assertSessionHasErrors('local_id');

       $this->patch(route('update-user', $carolina->id), ['local_id' => 111])
           ->assertSessionHasNoErrors();
   }

    /** @test */
    public function user_email_is_nullable()
    {
        $this->withExceptionHandling();
        $inscrey = create(Institution::class, ['name' => 'Instituto Cristo Rey']);
        $carolina = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id, 'local_id' => 111]);
        $luzma = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id, 'local_id' => 333]);
        $this->signIn($luzma);
        $this->patch(route('update-user', $carolina->id), ['email' => ''])
            ->assertSessionHasNoErrors('email');
    }

}
