<?php

namespace Tests\Feature\Users;

use App\Institution;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EditUserTest extends TestCase
{
    /** @test */
    public function admins_and_teachers_can_see_the_edit_page_of_students()
    {
        $inscrey = create(Institution::class, ['name' => 'Instituto Cristo Rey']);
        $elRosario = create(Institution::class, ['name' => 'Colegio el Rosario']);

        $rosaristaStudent = create(User::class, ['institution_id' => $elRosario]);

        $senoBlanca = create(User::class, ['type' => User::ADMIN_TYPE, 'institution_id' => $inscrey->id]);
        $divierPerez = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
        $luisMoreno = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
        $profeManuel = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);

        $currentInscreyUsers = [$senoBlanca, $divierPerez, $profeManuel];
        $editedInscreyUsers = [$senoBlanca, $luisMoreno, $profeManuel];

        foreach ($currentInscreyUsers as $currentUser) {
            $this->signIn($currentUser);
            $this->get(route('edit-user', [$rosaristaStudent->id]))
                ->assertStatus(403);
            foreach ($editedInscreyUsers as $editedUser) {
                if(in_array($currentUser->type, [User::ADMIN_TYPE, User::TEACHER_TYPE])) {
                    $this->get(route('edit-user', [$editedUser->id]))
                        ->assertStatus(200)
                        ->assertSee($editedUser->first_name)
                        ->assertSee($editedUser->last_name)
                        ->assertSee($editedUser->local_id)
                        ->assertSee($editedUser->email)
                        ->assertSee($editedUser->phone)
                        ->assertSee(__('copies.user_types.' . $editedUser->type));
                }

                if(in_array($currentUser->type, [User::STUDENT_TYPE])) {
                    $this->get(route('edit-user', [$editedUser->id]))
                        ->assertStatus(403);
                }

            }
        }

        $this->signIn($divierPerez);
        $this->get(route('edit-user', [$divierPerez->id]))
            ->assertStatus(200);

    }

}
