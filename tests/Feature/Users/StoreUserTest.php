<?php

namespace Tests\Feature\Users;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StoreUserTest extends TestCase
{
    /** @test */
    public function the_first_name_is_required()
    {
        $this->teacherSignIn();
        $this->postJson(route('store-user'), [])
            ->assertStatus(422)
            ->assertJsonValidationErrors('first_name');
    }

    /** @test */
    public function the_last_name_is_required()
    {
        $this->teacherSignIn();
        $this->postJson(route('store-user'), [])
            ->assertStatus(422)
            ->assertJsonValidationErrors('last_name');
    }

    /** @test */
    public function the_local_id_is_required()
    {
        $this->teacherSignIn();
        $this->postJson(route('store-user'), [])
            ->assertStatus(422)
            ->assertJsonValidationErrors('local_id');
    }

    /** @test */
    public function the_local_id_is_a_number()
    {
        $this->teacherSignIn();
        $this->postJson(route('store-user'), ['local_id' => 'not-a-number'])
            ->assertStatus(422)
            ->assertJsonValidationErrors('local_id');
    }

    /** @test */
    public function the_user_type_is_required()
    {
        $this->teacherSignIn();
        $this->postJson(route('store-user'), [])
            ->assertStatus(422)
            ->assertJsonValidationErrors('type');
    }

    /** @test */
    public function the_user_type_is_limited()
    {
        $this->adminSignIn();
        foreach (User::types() as $type) {
            $this->postJson(route('store-user'), [
                'first_name' => '',
                'last_name' => 'Moreno',
                'local_id' => 1120741,
                'type' => $type,
            ])
                ->assertStatus(422)
                ->assertJsonMissingValidationErrors('type');
        }
        foreach (['otro', 'tipo', 'invalido', 999, null] as $type) {
            $this->postJson(route('store-user'), [
                'first_name' => '',
                'last_name' => 'Moreno',
                'local_id' => 1120741,
                'type' => $type,
            ])
                ->assertStatus(422)
                ->assertJsonValidationErrors('type');
        }

    }


    /** @test */
    public function a_teacher_can_create_a_student()
    {
        $this->withoutExceptionHandling();
        $missMolina = $this->teacherSignIn();
        $this->postJson(route('store-user'), [
            'first_name' => 'Luis',
            'last_name' => 'Moreno',
            'local_id' => 1120741,
            'type' => User::STUDENT_TYPE,
        ])->assertRedirect()->assertSessionHas('success', __('copies.create_invitation.succes_message'));
        $luis = User::where('first_name', 'Luis')->where('last_name', 'Moreno')->first();
        $this->assertNotNull($luis);
        $this->assertNotNull($luis->login_token);
        $this->assertEquals($missMolina->institution_id, $luis->institution_id);
    }

    /** @test */
    public function a_teacher_can_only_create_students()
    {
        $missMolina = $this->teacherSignIn();
        foreach (User::types() as $type) {
            $data = [
                'first_name' => '',
                'last_name' => 'Moreno',
                'local_id' => 1120741,
                'type' => $type,
            ];
            if ($type == User::STUDENT_TYPE) {
                $this->postJson(route('store-user'), $data)
                    ->assertStatus(422)
                    ->assertJsonMissingValidationErrors('type');
            } else {
                $this->postJson(route('store-user'), $data)
                    ->assertStatus(422)
                    ->assertJsonValidationErrors('type');
            }

        }
    }
    /** @test */
    public function an_admin_can_create_any_type_of_user()
    {
        $missMolina = $this->adminSignIn();
        foreach (User::types() as $type) {
            $data = [
                'first_name' => '',
                'last_name' => 'Moreno',
                'local_id' => 1120741,
                'type' => $type,
            ];
            $this->postJson(route('store-user'), $data)
                ->assertStatus(422)
                ->assertJsonMissingValidationErrors('type');

        }
    }



}
