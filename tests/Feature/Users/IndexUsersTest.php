<?php

namespace Tests\Feature\Users;

use App\Http\Livewire\Users\Index;
use App\Institution;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Livewire;

class IndexUsersTest extends TestCase
{
    /** @test */
    public function the_users_index_required_authentication()
    {
        $this->getJson(route('index-users'))->assertStatus(401);
        $this->get(route('index-users'))->assertRedirect(route('login'));
    }

    /** @test */
    public function the_index_show_users()
    {
        $inscrey = create(Institution::class);
        $luis = create(User::class, ['institution_id' => $inscrey->id, 'first_name' => 'luis']);
        $di = create(User::class, ['institution_id' => $inscrey->id, 'first_name' => 'divier']);

        $this->signIn($luis);
        Livewire::test(Index::class)->assertSee('luis')->assertSee('divier');
    }

    /** @test */
    public function the_index_dont_show_users_of_another_institution()
    {
        $inscrey = create(Institution::class);
        $luis = create(User::class, ['institution_id' => $inscrey->id, 'first_name' => 'luis']);
        $di = create(User::class, ['institution_id' => $inscrey->id, 'first_name' => 'divier']);
        $margarita = create(User::class, ['first_name' => 'margarita']);

        $this->signIn($luis);
        Livewire::test(Index::class)->assertDontSee('margarita');
    }



}
