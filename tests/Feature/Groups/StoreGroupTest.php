<?php

namespace Tests\Feature\Groups;

use App\Group;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StoreGroupTest extends TestCase
{
    /** @test */
    public function a_group_can_be_stored_only_if_logged_in()
    {
        $this->postJson(route('store-group'), ['name' => 'Math', 'year' => 2020, 'period' => '2'])->assertStatus(401);
    }

    /** @test */
    public function a_teacher_can_store_a_group()
    {
        $profe = $this->teacherSignIn();
        $group = Group::first();
        $this->assertNull($group);
        $response = $this->post(route('store-group'), ['name' => 'Math', 'year' => 2020, 'period' => 2])
            ->assertRedirect()
            ->assertSessionHas(['success' => __('copies.store_group.success_message')]);
        $group = Group::first();
        $this->assertNotNull($group);
        $this->assertEquals($profe->id, $group->creator->id);
        $response->assertRedirect(route('show-group', [$group->id]));
    }

    /** @test */
    public function a_student_cant_store_a_group()
    {
        $divierPerez = $this->studentSignIn();
        $group = Group::first();
        $this->assertNull($group);
        $response = $this->post(route('store-group'), ['name' => 'Math', 'year' => 2020, 'period' => 2])
            ->assertStatus(403);
        $group = Group::first();
        $this->assertNull($group);
    }
}
