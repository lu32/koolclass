<?php

namespace Tests\Feature\Groups;

use App\Group;
use App\Institution;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AddStudentToGroupTest extends TestCase
{
    /** @test */
    public function a_teacher_can_add_students_to_groups()
    {
        $inscrey = create(Institution::class);
        $missMolina = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);
        $divierPerez = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
        $this->signIn($missMolina);
        $tercero2 = create(Group::class, ['institution_id' => $inscrey->id, 'name' => 'tercero 2']);
        $this->assertCount(0, $tercero2->students);
        $this->post(route('add-student-to-group', $tercero2->id), ['user_id' => $divierPerez->id])
              ->assertRedirect(route('show-group', $tercero2));
        $this->assertCount(1, $tercero2->fresh()->students);
    }

    /** @test */
    public function a_student_cant_add_students_to_groups()
    {
        $this->withoutExceptionHandling();
        $inscrey = create(Institution::class);
        $missMolina = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);
        $divierPerez = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
        $this->signIn($divierPerez);
        $tercero2 = create(Group::class, ['institution_id' => $inscrey->id, 'name' => 'tercero 2']);
        $this->assertCount(0, $tercero2->students);
        $this->post(route('add-student-to-group', $tercero2->id), ['user_id' => $divierPerez->id])
            ->assertRedirect(route('show-group', $tercero2))
            ->assertSessionHas(['error' => __('copies.group_students.cant_add_to_group')]);
        $this->assertCount(0, $tercero2->fresh()->students);
    }

    /** @test */
    public function it_show_session_errors_in_groups_students_page()
    {
        $inscrey = create(Institution::class);
        $missMolina = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);
        $this->signIn($missMolina);
        $tercero2 = create(Group::class, ['institution_id' => $inscrey->id, 'name' => 'tercero 2']);
        $this->withSession(['error' => __('copies.group_students.cant_add_to_group')])
            ->get(route('group-students', $tercero2->id))
            ->assertSee(__('copies.group_students.cant_add_to_group'));

    }

    /** @test */
    public function users_from_another_institution_cant_add_students_to_groups()
    {
        $inscrey = create(Institution::class);
        $buenConsejo = create(Institution::class);
        $missRaquel = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $buenConsejo->id]);
        $missMolina = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);
        $divierPerez = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
        $this->signIn($missRaquel);
        $tercero2 = create(Group::class, ['institution_id' => $inscrey->id, 'name' => 'tercero 2']);
        $this->assertCount(0, $tercero2->students);
        $this->post(route('add-student-to-group', $tercero2->id), ['user_id' => $divierPerez->id])
            ->assertRedirect(route('show-group', $tercero2))
            ->assertSessionHas(['error' => __('copies.group_students.cant_add_to_group')]);
        $this->assertCount(0, $tercero2->fresh()->students);
    }

    /** @test */
    public function users_from_another_institution_cant_add_students_of_my_institution_to_their_groups()
    {
        $this->withoutExceptionHandling();
        $inscrey = create(Institution::class);
        $buenConsejo = create(Institution::class);
        $missRaquel = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $buenConsejo->id]);
        $missMolina = create(User::class, ['type' => User::TEACHER_TYPE, 'institution_id' => $inscrey->id]);
        $divierPerez = create(User::class, ['type' => User::STUDENT_TYPE, 'institution_id' => $inscrey->id]);
        $this->signIn($missRaquel);
        $cuarto = create(Group::class, ['institution_id' => $buenConsejo->id, 'name' => 'Cuarto']);
        $this->assertCount(0, $cuarto->students);

        $this->post(route('add-student-to-group', $cuarto->id), ['user_id' => $divierPerez->id])
            ->assertRedirect(route('show-group', $cuarto))
            ->assertSessionHas(['error' => __('copies.group_students.cant_add_to_group')]);

        $this->assertCount(0, $cuarto->fresh()->students);
    }




}
