<?php

namespace Tests\Feature\Groups;

use App\Group;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GroupCoursesFormTest extends TestCase
{
   /** @test */
   public function a_user_can_see_the_add_subject_form()
   {
       $missMolina = $this->signIn();
       $group = create(Group::class, ['creator_id' => $missMolina->id]);
       $this->get(route('add-subject', [$group->id]))->assertStatus(200);
   }

}
