<?php

namespace Tests\Feature\Groups;

use App\Group;
use App\Institution;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GroupStudentsTest extends TestCase
{
    /** @test */
    public function it_required_auth()
    {
        $inscrey = create(Institution::class);
        $segundoInscrey = create(Group::class, ['institution_id' => $inscrey->id]);
        $this->get(route('group-students', $segundoInscrey->id))
            ->assertRedirect(route('login'));
    }
    
   /** @test */
   public function it_can_visit_the_group_students_page()
   {
       $inscrey = create(Institution::class);
       $inscreyStudent = create(User::class,
           [
               'type' => User::STUDENT_TYPE,
               'institution_id' => $inscrey->id
           ]);
       $segundoInscrey = create(Group::class, ['institution_id' => $inscrey->id]);
       $this->signIn($inscreyStudent);
       $this->get(route('group-students', $segundoInscrey->id))
           ->assertStatus(200);
   }

   /** @test */
   public function another_institution_student_cant_visit_the_groups_page()
   {
       $inscrey = create(Institution::class);
       $inscreyStudent = create(User::class,
           [
               'type' => User::STUDENT_TYPE,
               'institution_id' => $inscrey->id
           ]);
       $segundoInscrey = create(Group::class, ['institution_id' => $inscrey->id]);
       $rosarioStudent = $this->studentSignIn();
       $this->get(route('group-students', $segundoInscrey->id))->assertStatus(403);
   }

   /** @test */
   public function it_shows_the_groups_students_names()
   {
       $this->withoutExceptionHandling();
       $inscrey = create(Institution::class);
       $inscreyStudents = create(User::class,
           [
               'type' => User::STUDENT_TYPE,
               'institution_id' => $inscrey->id
           ], 5);
       $segundoGradoInscrey = create(Group::class, ['institution_id' => $inscrey->id]);
       $segundoGradoInscrey->students()->attach($inscreyStudents);
       $this->signIn($inscreyStudents[0]);
       foreach ($inscreyStudents as $student) {
           $this->get(route('group-students', $segundoGradoInscrey->id))
               ->assertStatus(200)
               ->assertSee($student->first_name)->assertSee($student->last_name);
       }

   }



}
