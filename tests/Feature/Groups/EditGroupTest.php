<?php

namespace Tests\Feature\Groups;

use \App\Group;
use \App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EditGroupTest extends TestCase
{
    /** @test */
    public function a_user_can_see_the_edit_form_of_his_group()
    {
        $this->withoutExceptionHandling();
        $teacher = $this->signIn();
        $group = create(Group::class, ['creator_id' => $teacher->id]);
        $this->get(route('edit-group', [$group->id]))
            ->assertStatus(200)
            ->assertSee(route('show-group', [$group->id]))
            ->assertSee(route('update-group', [$group->id]));
    }

    /** @test */
    public function a_guest_cant_see_the_edit_page()
    {
        $group = create(Group::class);
        $this->getJson(route('edit-group', [$group->id]))
            ->assertStatus(401);
    }

    /** @test */
    public function a_teacher_cant_see_anothers_teacher_edit_group_page()
    {
        $buenConsejoTeacher = $this->signIn();
        $cristoReyTeacher = create(User::class);
        $group = create(Group::class, ['creator_id' => $cristoReyTeacher->id]);
        $this->getJson(route('edit-group', [$group->id]))
            ->assertStatus(403);
    }

}
