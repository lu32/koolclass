<?php

namespace Tests\Feature\Groups;

use \App\Group;
use App\Http\Livewire\CoursesIndex;
use App\Http\Livewire\Groups\GroupsIndex;
use \App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Livewire;

class IndexGroupsTest extends TestCase
{
    /** @test */
    public function a_teacher_can_see_his_institution_groups_index()
    {
        $missMolina = $this->signIn();
        for($i=0; $i<50; $i++){
            $groups = create(Group::class, [
                'creator_id' => $missMolina->id,
            ]);
        }

        $this->get(route('index-groups'))
            ->assertStatus(200)
            ->assertSeeText(__('copies.index_groups.card_title'));
            // ->assertSeeTextInOrder($groups->pluck('name')->all());
    }

    /** @test */
    public function a_teacher_cant_see_another_institution_groups()
    {
        $this->withoutExceptionHandling();
        $missMolina = $this->signIn();
        $misterCordoba = create(User::class);
        $uniqueNames1 = [
            'Primero', 'Segundo', 'Tercero', 'Cuarto', 'Quinto'
        ];
        $uniqueNames2 = [
            'Sexto', 'Séptimo', 'Octavo', 'Noveno', 'Décimo'
        ];
        for($i=0; $i<5; $i++){
            create(Group::class, ['creator_id' => $missMolina->id, 'name' => $uniqueNames1[$i]]);
            create(Group::class, ['creator_id' => $misterCordoba->id, 'name' => $uniqueNames2[$i]]);
        }
        $missMolinaInstitutionGroups = $missMolina->institution->groups;
        $misterCordobaInstitutionGroups = $misterCordoba->institution->groups;
        foreach ($misterCordobaInstitutionGroups as $misterCordobaGroup) {
            $this->get(route('index-groups'))
                ->assertStatus(200)
                ->assertDontSeeText($misterCordobaGroup->name);
        }
        foreach ($missMolinaInstitutionGroups as $missMolinaGroup) {
            $this->get(route('index-groups'))
                ->assertStatus(200)
                ->assertSeeText($missMolinaGroup->name);
        }
    }

    /** @test */
    public function a_guest_cant_see_the_groups_index()
    {
        $groups = create(Group::class, [], 50);
        $this->getJson(route('index-groups'))
            ->assertStatus(401);
        $this->get(route('index-groups'))
            ->assertRedirect(route('login'));
    }

    /** @test */
    public function students_cant_see_the_create_group_button()
    {
        $di = $this->studentSignIn();
        Livewire::test(GroupsIndex::class)->assertDontSee('create-group-button');
    }

    /** @test */
    public function teachers_can_see_the_create_group_button()
    {
        $julio = $this->teacherSignIn();
        Livewire::test(GroupsIndex::class)->assertSee('create-group-button');
    }

}
