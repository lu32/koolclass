<?php

namespace Tests\Feature\Groups;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateGroupTest extends TestCase
{
    /** @test */
    public function a_teacher_can_see_the_create_group_view()
    {
        $misterCarlos = $this->signIn();
        $this->get(route('create-group'))
            ->assertStatus(200)
            ->assertSee(route('store-group'))
            ->assertSee(__('copies.create_group.card_title'));
    }

    /** @test */
    public function a_student_can_see_the_create_group_view()
    {
        $divierPerez = $this->studentSignIn();
        $this->get(route('create-group'))
            ->assertStatus(403);
    }

    /** @test */
    public function a_guest_user_cant_see_the_create_group_view() {
        $this->getJson(route('create-group'))
            ->assertStatus(401);
        $this->get(route('create-group'))
            ->assertRedirect(route('login'));
    }
}
