<?php

namespace Tests\Feature\Groups;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Group;
use App\User;

class UpdateGroupTest extends TestCase
{
    /** @test */
    public function a_user_can_update_his_group()
    {
        $this->withoutExceptionHandling();
        $profe = $this->signIn();
        $group = create(Group::class, ['creator_id' => $profe->id, 'name' => '10th grade']);
        $this->patch(route('update-group', [$group->id]), ['name' => '8th grade'])
            ->assertStatus(302)
            ->assertSessionHas('success', __('copies.update_group.success_message'));
        $this->assertEquals('8th grade', $group->fresh()->name);
    }

    /** @test */
    public function a_guest_user_cant_update_groups()
    {
        $group = create(Group::class, ['name' => '6th grade']);
        $this->patch(route('update-group', [$group->id]), ['name' => '3rd grade'])
            ->assertStatus(302)
            ->assertRedirect(route('login'));
        $this->patchJson(route('update-group', [$group->id]), ['name' => '3rd grade'])
            ->assertStatus(401);
        $this->assertEquals('6th grade', $group->fresh()->name);
    }

    /** @test */
    public function a_teacher_cant_update_anothers_teacher_group()
    {
        $cristoReyTeacher = create(User::class);
        $group = create(Group::class, ['creator_id' => $cristoReyTeacher->id, 'name' => '6th grade']);
        $buenConsejoTeacher = $this->signIn();
        $this->patchJson(route('update-group', [$group->id]), ['name' => '3rd grade'])
            ->assertStatus(403);
        $this->assertEquals('6th grade', $group->fresh()->name);
    }
}
