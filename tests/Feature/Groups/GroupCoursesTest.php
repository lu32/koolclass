<?php

namespace Tests\Feature\Groups;

use App\Course;
use App\Group;
use App\PeriodsCollection;
use App\User;
use App\Folder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GroupCoursesTest extends TestCase
{
    /** @test */
    public function a_user_only_sees_his_own_period_courses_for_adding_to_a_group()
    {
        $missMolina = $this->signIn();
        $mrCordoba = create(User::class, ['first_name' => 'Julio Cesar Cordoba']);
        $terceroB = create(Group::class, ['name' => 'tercero B', 'year' => 2020, 'period' => PeriodsCollection::PERIOD_ALL_YEAR]);
        $missMolinaAllYearGeography = create(Course::class, [
            'name' => 'Geography third grade 2020 miss molinpa all year',
            'year' => 2020,
            'period' => PeriodsCollection::PERIOD_ALL_YEAR,
            'creator_id' => $missMolina->id
        ]);
        create(Folder::class, ['course_id' => $missMolinaAllYearGeography->id]);
        $missMolinaInterSemesterGeography = create(Course::class, [
            'name' => 'Geography third grade 2020 miss molina intersemester',
            'year' => 2020,
            'period' => PeriodsCollection::PERIOD_INTERSEMESTER,
            'creator_id' => $missMolina->id
        ]);
        create(Folder::class, ['course_id' => $missMolinaInterSemesterGeography->id]);
        $mrCordobaAllYearGeography = create(Course::class, [
            'name' => 'Geography third grade 2020 mr cordoba',
            'year' => 2020,
            'period' => PeriodsCollection::PERIOD_ALL_YEAR,
            'creator_id' => $mrCordoba->id
        ]);
        create(Folder::class, ['course_id' => $mrCordobaAllYearGeography->id]);

        $this->get(route('add-subject', [$terceroB->id]))
            ->assertStatus(200)
            ->assertDontSeeText($missMolinaInterSemesterGeography->name)
            ->assertDontSeeText($mrCordobaAllYearGeography->name)
            ->assertSeeText($missMolinaAllYearGeography->name);


    }

    /** @test */
    public function empty_list_message_for_no_courses_available_is_shown()
    {
        $missMolina = $this->signIn();
        $terceroB = create(Group::class, ['name' => 'tercero B', 'year' => 2020, 'period' => PeriodsCollection::PERIOD_ALL_YEAR]);
        $this->get(route('add-subject', [$terceroB->id]))
            ->assertStatus(200)
            ->assertSeeText(__('copies.group_courses.add_form.empty_courses_option'));
    }

    /** @test */
    public function empty_list_group_courses_redirect_back()
    {
        $missMolina = $this->signIn();
        $terceroB = create(Group::class, ['name' => 'tercero B', 'year' => 2020, 'period' => PeriodsCollection::PERIOD_ALL_YEAR]);
        $this->post(route('group-courses', [$terceroB->id]), ['course_id' => ''])
            ->assertStatus(302)
            ->assertSessionHasErrors('course_id');

    }

    /** @test */
    public function a_teacher_can_add_courses_to_the_groups()
    {
        $missMolina = $this->signIn();
        $mrCordoba = create(User::class, ['first_name' => 'Julio Cesar Cordoba']);
        $terceroB = create(Group::class, ['name' => 'tercero B', 'year' => 2020, 'period' => PeriodsCollection::PERIOD_ALL_YEAR]);
        $missMolinaAllYearGeography = create(Course::class, [
            'name' => 'Geography third grade 2020 miss molina all year',
            'year' => 2020,
            'period' => PeriodsCollection::PERIOD_ALL_YEAR,
            'creator_id' => $missMolina->id
        ]);
        $this->assertCount(0, $terceroB->courses);
        $this->post(route('group-courses', [$terceroB->id]), ['course_id' => $missMolinaAllYearGeography->id])
            ->assertRedirect(route('show-group', [$terceroB->id]))
            ->assertSessionHas('success', __('copies.group_courses.course_added_message') . " $terceroB->name");
        $this->assertCount(1, $terceroB->fresh()->courses);
    }

    /** @test */
    public function a_teacher_cant_add_another_teacher_courses_to_a_group()
    {
        $missMolina = $this->signIn();
        $mrCordoba = create(User::class, ['first_name' => 'Julio Cesar Cordoba']);
        $terceroB = create(Group::class, ['name' => 'tercero B', 'year' => 2020, 'period' => PeriodsCollection::PERIOD_ALL_YEAR]);
        $missMolinaAllYearGeography = create(Course::class, [
            'name' => 'Geography third grade 2020 miss molina all year',
            'year' => 2020,
            'period' => PeriodsCollection::PERIOD_ALL_YEAR,
            'creator_id' => $mrCordoba->id
        ]);
        $this->assertCount(0, $terceroB->courses);
        $this->post(route('group-courses', [$terceroB->id]), ['course_id' => $missMolinaAllYearGeography->id])
            ->assertStatus(403);
        $this->assertCount(0, $terceroB->fresh()->courses);
    }

    /** @test */
    public function cant_add_a_course_twice_or_more()
    {
        $missMolina = $this->signIn();
        $mrCordoba = create(User::class, ['first_name' => 'Julio Cesar Cordoba']);
        $terceroB = create(Group::class, ['name' => 'tercero B', 'year' => 2020, 'period' => PeriodsCollection::PERIOD_ALL_YEAR]);
        $missMolinaAllYearGeography = create(Course::class, [
            'name' => 'Geography third grade 2020 miss molina all year',
            'year' => 2020,
            'period' => PeriodsCollection::PERIOD_ALL_YEAR,
            'creator_id' => $missMolina->id
        ]);
        $this->assertCount(0, $terceroB->courses);
        $this->post(route('group-courses', [$terceroB->id]), ['course_id' => $missMolinaAllYearGeography->id])
            ->assertRedirect(route('show-group', [$terceroB->id]))
            ->assertSessionHas('success', __('copies.group_courses.course_added_message') . " $terceroB->name");
        $this->assertCount(1, $terceroB->fresh()->courses);

        $this->post(route('group-courses', [$terceroB->id]), ['course_id' => $missMolinaAllYearGeography->id])
            ->assertRedirect(route('show-group', [$terceroB->id]))
            ->assertSessionHas('success', __('copies.group_courses.course_added_message') . " $terceroB->name");
        $this->assertCount(1, $terceroB->fresh()->courses);
    }
}
