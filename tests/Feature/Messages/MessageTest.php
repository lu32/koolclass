<?php

namespace Tests\Feature\Messages;

use App\Message;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MessageTest extends TestCase
{
    /** @test */
    public function the_message_body_is_sanitized()
    {
        $message = create(Message::class, ['body' => '<script>alert("Bad!")</script><p>The good part</p>']);
        $this->assertEquals('<p>The good part</p>', $message->body);
    }

}
