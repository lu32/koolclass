<?php

use Illuminate\Database\Seeder;

class World1 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sanFrancisco = App\Institution::create(['name' => 'I.E.D.SAN FRANCISCO JAVIER']);
        $damelys = create(\App\User::class, [
            'email' => 'damoca@koolclass.com',
            'first_name' => 'Damelys',
            'last_name' => 'Molina',
            'institution_id' => $sanFrancisco->id,
            'type' => \App\User::TEACHER_TYPE
        ]);
        $terceroB = \App\Group::create([
            'name' => 'Tercero 2',
            'institution_id' => $sanFrancisco->id,
            'year' => 2020,
            'period' => 0,
            'creator_id' => $damelys->id,
        ]);
        $studentsData = [
            [
                'first_name' => 'Ana Lucia',
                'last_name' => 'Romero',
                'institution_id' => $sanFrancisco->id,
                'local_id' => '11201',
                'type' => \App\User::STUDENT_TYPE,
            ],
            [
                'first_name' => 'Jesus Leonardo',
                'last_name' => 'Chavez',
                'institution_id' => $sanFrancisco->id,
                'local_id' => '11202',
                'type' => \App\User::STUDENT_TYPE,
            ],
            [
                'first_name' => 'Juan Jose',
                'last_name' => 'Soto',
                'institution_id' => $sanFrancisco->id,
                'local_id' => '11203',
                'type' => \App\User::STUDENT_TYPE,
            ]
        ];
        $students = collect();
        collect($studentsData)->each(function($student) use($terceroB) {
            $created = \App\User::create($student);
            $terceroB->students()->attach($created);
        });

    }
}
