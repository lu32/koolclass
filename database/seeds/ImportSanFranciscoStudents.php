<?php

use App\User;
use Illuminate\Database\Seeder;

class ImportSanFranciscoStudents extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //CAN,DOC,TIPODOC,APELLIDO1,APELLIDO2,NOMBRE1,NOMBRE2,GENERO,FECHA_NACIMIENTO,EDAD
        $institution_id = 1;
        if (($handle = fopen(resource_path('files/san francisco estudiantes.csv'), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $first_name = "$data[5] $data[6]";
                $last_name = "$data[3] $data[4]";
                $local_id = $data[1];
                $type = User::STUDENT_TYPE;
                $local_id_type = $data[2];
                $gender = $data[7];
                $birth_date = $data[8];
                User::create(compact('first_name', 'last_name', 'local_id', 'type', 'institution_id'));
            }

            fclose($handle);
        }
    }
}
