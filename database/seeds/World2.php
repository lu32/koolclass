<?php

use Illuminate\Database\Seeder;

class World2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sanFrancisco = App\Institution::create(['name' => 'I.E.D.SAN FRANCISCO JAVIER']);
        $damelys = create(\App\User::class, [
            'email' => 'damoca@koolclass.com',
            'first_name' => 'Damelys',
            'last_name' => 'Molina',
            'local_id' => '69656788',
            'institution_id' => $sanFrancisco->id,
            'type' => \App\User::ADMIN_TYPE
        ]);

        $manuelGuerra = create(\App\User::class, [
            'email' => 'manuel@koolclass.com',
            'first_name' => 'Manuel',
            'last_name' => 'Guerra',
            'local_id' => '4431098',
            'institution_id' => $sanFrancisco->id,
            'type' => \App\User::TEACHER_TYPE
        ]);

        $divier = create(\App\User::class, [
            'email' => 'divier@koolclass.com',
            'first_name' => 'Divier',
            'last_name' => 'Perez',
            'local_id' => '1120766409',
            'institution_id' => $sanFrancisco->id,
            'type' => \App\User::STUDENT_TYPE
        ]);


        $tercero1 = \App\Group::create([
            'name' => 'Tercero 1',
            'institution_id' => $sanFrancisco->id,
            'year' => 2020,
            'period' => 0,
            'creator_id' => $manuelGuerra->id,
        ]);

        $tercero2 = \App\Group::create([
            'name' => 'Tercero 2',
            'institution_id' => $sanFrancisco->id,
            'year' => 2020,
            'period' => 0,
            'creator_id' => $damelys->id,
        ]);

        $this->call(ImportSanFranciscoStudents::class);

        \App\User::whereType(\App\User::STUDENT_TYPE)
            ->chunk(30, function($students, $chunk) use($tercero1, $tercero2) {
                if ($chunk == 1) {
                    $tercero1->students()->sync($students->pluck('id')->all());
                }

                if ($chunk == 2) {
                    $tercero2->students()->sync($students->pluck('id')->all());
                }

                if($chunk > 2) {

                    return false;
                }
            });

        $courses = ['Matematicas', 'Español', 'Biologia', 'Etica y Religion', 'Informatica'];
        foreach ($courses as $course) {
            \App\Course::create([
               'name' => $course,
                'creator_id' => $manuelGuerra->id,
                'year' => '2020',
                'period' => '0',
                'institution_id' => $sanFrancisco->id,
            ]);
        }
        foreach ($courses as $course) {
            \App\Course::create([
                'name' => $course,
                'creator_id' => $damelys->id,
                'year' => '2020',
                'period' => '0',
                'institution_id' => $sanFrancisco->id,
            ]);
        }

    }
}
