<?php

use Illuminate\Database\Seeder;
use \App\Group;
use \App\Institution;
use \App\User;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Institution::all() as $institution){
            $teachers = $institution->teachers;
            $teacherId = $teachers[array_rand($teachers->toArray())]->id;
            for($i=0; $i<rand(0,20); $i++){
                $year = rand(2020,2030);
                $period = rand(0,3);
                $name = rand(1,11).'°'.chr(rand(65,68));

                #Counter to limit the number of tries inside the while loop
                $tries = 0;

                #If the tuple ($name, $year, $period, $institution->id) already exists in the columns ('name', 'year', 'period', 'institution_id')
                #then change the variable $name until the tuple is once again unique. If after 1000 tries this couldn't be achieved
                #then give up
                while(!Group::where('institution_id', $institution->id)
                    ->where('year', $year)
                    ->where('period', $period)
                    ->where('name', $name)
                    ->get()
                    ->isEmpty() && $tries < 1000){
                        $tries++;
                        $name = rand(1,11).'°'.chr(rand(65,68));
                }

                #If a unique tuple couldn't be generated, then change the value of $name to a new one matching the following pattern
                if($tries===1000){
                    $name = rand(12,99).'°'.chr(rand(69,90));
                }

               create(Group::class, ['name' => $name, 'year' => $year, 'period' => $period, 'creator_id' => $teacherId]); 
            }
        }
    }
}
