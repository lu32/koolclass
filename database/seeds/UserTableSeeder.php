<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        create(\App\User::class, [
            'email' => 'damoca@koolclass.com',
            'first_name' => 'Damelys',
            'last_name' => 'Molina',
            'institution_id' => 1,
            'type' => \App\User::TEACHER_TYPE
        ]);

        foreach(App\Institution::all() as $institution){
            create(App\User::class,['institution_id' => $institution->id], rand(2,10));
        }
    }
}
