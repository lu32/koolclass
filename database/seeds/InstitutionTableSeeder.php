<?php

use Illuminate\Database\Seeder;

class InstitutionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'I.E.D.SAN FRANCISCO JAVIER',
            'Colegio Bilingüe Diana Oese',
            'Colegio el Rosario',
            'Coregio Nueva Granada',
            'Colegio Marymount'
        ];

        foreach($names as $name) {
            App\Institution::create(compact('name'));
        }
    }
}
