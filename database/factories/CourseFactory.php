<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'creator_id' => function() {
            return create(\App\User::class)->id;
        },
        'institution_id' => function() {
            return create(\App\Institution::class)->id;
        },
        'year' => $faker->year(),
        'period' => $faker->randomElement(
            collect(\App\PeriodsCollection::all())->filter(function($item) { return $item;})->all()
        ),
    ];
});
