<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \App\Group;
use \App\User;
use Faker\Generator as Faker;

$factory->define(Group::class, function (Faker $faker, $params) {
    return [
        'name' => $faker->unique()->word(),
        'year' => $faker->year(),
        'period' => $faker->randomElement(
            collect(\App\PeriodsCollection::all())->filter(function($item) { return $item;})->all()
        ),
        'creator_id' => function() {
            return create(User::class)->id;
        },
        'institution_id' => function($attributes) {
            return User::find($attributes['creator_id'])->institution_id;
        }
    ];
});


