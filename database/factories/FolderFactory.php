<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Folder;
use App\Course;
use Faker\Generator as Faker;

$factory->define(Folder::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'course_id' => function() {
            return create(Course::class)->id;
        },
    ];
});
