<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Upload;
use Faker\Generator as Faker;

$factory->define(Upload::class, function (Faker $faker) {
    $extension = $faker->fileExtension;
    return [
        'name' => $faker->word . ".{$extension}",
        'extension' => $extension,
        'size' => $faker->numberBetween(100),
        'path' => "/uploads/{$faker->uuid}.{$faker->fileExtension}"
    ];
});
