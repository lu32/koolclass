<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name');
            $table->unsignedSmallInteger('year');
            $table->smallInteger('period');

            $table->foreignId('institution_id');
            $table->foreign('institution_id')->references('id')->on('institutions');

            $table->foreignId('creator_id');
            $table->foreign('creator_id')->references('id')->on('users');

            $table->unique(['name', 'year', 'period', 'institution_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
