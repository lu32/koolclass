const mix = require('laravel-mix');
let atImport = require('postcss-import');
require('laravel-mix-purgecss');



/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/build')
    .postCss('resources/css/app.css', 'public/build/app.css', [
        atImport(),
        require('tailwindcss')
    ])
    .purgeCss()
    .version().sourceMaps();
