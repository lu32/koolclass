<?php

return [
    'home' => [
        'welcome_title' => [
            'first_part' => 'Una plataforma web para',
            'second_part' => 'la enseñanza remota de alta calidad',
        ],
        'welcome_message' => 'Koolclass es una plataforma web, especializada en educación de niños de primaria y jovenes de bachillerato, provee los bloques de construcción para el proceso de aprendizaje remoto sin dificultad de uso ni complejidad.',
    ],
    'breadcrumbs' => [
        'dashboard' => 'Inicio',
        'index_courses' => 'Materias',
        'index_groups' => 'Grupos',
        'group_subjects' => 'Materias',
        'index_users' => 'Usuarios',
        'course_messages' => 'Mensajes',
    ],
    'header' => [
        'dashboard_link' => 'Inicio',
        'courses_link' => 'Materias',
        'groups_link' => 'Grupos',
        'login_link' => 'Entrar',
        'register_link' => 'Registrarse',
        'logout_link' => 'Salir',
        'profile_link' => 'Perfil',
    ],
    'dashboard' => [
        'cart_title' => 'Tablero',
        'profile_link' => 'Mi perfil',
        'courses_link' => 'Materias',
        'groups_link' => 'Grupos',
        'users_link' => 'Estudiantes y profesores',
        'first_login_welcome_message' => 'Bienvenido a Koolclass',
    ],

    #Registration
    'register' => [
        'card_title' => 'Registrarse',
        'name_label' => 'Tu nombre',
        'last_label' => 'Apellido',
        'username_label' => 'Username',
        'email_label' => 'Correo electrónico',
        'password_label' => 'Contraseña',
        'confirm_password_label' => 'Confirmar contraseña',
        'institution_selector_label' => 'Institución a la que perteneces:',
        'submit_button' => 'Registrarse'
    ],

    #Courses
    'create_course' => [
        'card_title' => 'Crear materia',
        'name_label' => 'Materia',
        'name_placeholder' => 'ej. Matematicas',
        'submit_button' => 'Crear',

    ],
    'store_course' => [
        'success_message' => 'Curso creado!'
    ],
    'edit_course' => [
        'title_prefix' => 'Editar el curso',
        'submit_button' => 'Actualizar'
    ],
    'update_course' => [
        'success_message' => '¡Curso actualizado con éxito!',
    ],
    'index_courses' => [
        'card_title' => 'Materias',
        'new_button' => 'Crear'
    ],
    'show_course' => [
        'files_card_title' => 'Archivos',
        'filters' => [
            'year_label' => 'Año',
            'period_label' => 'Periodo',
            'teacher_label' => 'Profesor',
        ],
        'list_headers' => [
            'course_name' => 'Curso',
            'creator' => 'Profesor',
            'year' => 'Año',
            'period' => 'Periodo',
        ],
    ],
    'course_files' => [
        'card_title' => 'Archivos',
        'upload_button' => 'Nuevo...',
        'empty_message' => [
            'first_part' => 'No hay archivos en esta carpeta, puedes',
            'second_part' => 'subir',
            'third_part' => 'y organizar archivos y material de referencia para la materia',
        ]
    ],
    'course_messages' => [
        'card_title' => 'Mensajes',
        'create_link' => 'Nuevo mensaje',
    ],

    'create_message' => [
        'save_button' => 'Guardar'
    ],

    #Groups
    'index_groups' => [
        'card_title' => 'Grupos',
        'new_button' => 'Crear',
    ],
    'create_group' => [
        'card_title' => 'Crear un nuevo grupo',
        'name_label' => 'Grupo',
        'name_placeholder' => 'p. ej. Sexto B',
        'submit_button' => 'Guardar',
        'period_selector_label' => 'Período:'
    ],
    'store_group' => [
        'success_message' => '¡Grupo creado!'
    ],
    'show_group' => [
        'files_card_title' => 'Estudiantes',
        'courses_link' => 'Materias',
        'courses_link' => 'Materias',
        'students_link' => 'Estudiantes',
    ],
    'edit_group' => [
        'title_prefix' => 'Editar el grupo',
        'submit_button' => 'Actualizar'
    ],
    'update_group' => [
        'success_message' => '¡Nombre del grupo actualizado con éxito!',
    ],
    'year' => [
        'year_selector_label' => 'Año'
    ],
    'period' => [
        'list' => [
            'select' => 'Seleccionar',
            0 => 'Todo el año',
            1 => 'Semestre 1',
            2 => 'Semestre 2',
            3 => 'Vacaciones'
        ],
        'period_selector_label' => 'Período:'
    ],

    'group_students' => [
        'students_cant_add' => 'Un estudiante no puede agregar otros estudiantes a un grupo',
        'cant_add_to_group' => 'No puedes agregar estudiantes a este grupo',
    ],


    #Folders
    'create_folder' => [
        'submit_button' => 'Crear',
        'cancel_button' => 'Cancelar',
        'name_label' => 'Nombre de la carpeta'
    ],
    'store_folder' => [
        'success_message' => 'Carpeta creada!'
    ],

    'group_courses' => [
        'card_title_prefix' => 'Materias',
        'add_form' => [
            'year_label' => 'Año',
            'period_label' => 'Periodo',
            'my_courses_label' => 'Materia',
            'empty_courses_option' => 'Sin cursos para el periodo',
            'first_option' => 'Selecciona una materia',
            'submit_button' => 'Agregar materia a'
        ],
        'course_added_message' => 'Materia agregada a',
        'validation' => [
            'not_your_course_error' => 'No puedes agregar cursos de otro profesor',
        ],
//        'empty_courses_message' => 'Este curso aún no tiene materias, agrega una arriba ☝️'
    ],

    #Users
    'create_invitation' => [
        'card_title' => 'Invitar integrante',
        'form' => [
            'first_name_label' => 'Nombre',
            'first_name_placeholder' => 'ej: Diana Sofia',
            'last_name_label' => 'Apellido',
            'last_name_placeholder' => 'ej: Montiel Molina',
            'local_id_label' => 'Numero de identidad',
            'local_id_placeholder' => 'ej: 1120777829',
            'email_label' => 'Correo electrónico',
            'email_placeholder' => 'ej: diana@colegiocrisorey.com',
            'phone_label' => 'Telefono',
            'phone_placeholder' => 'ej: 3017775555',
        ],
        'succes_message' => 'Integrante creado!',
        'validation_errors' => [
            'teacher_can_only_create_students' => 'Un profesor solo puede crear estudiantes'
        ]
    ],
    'user_types' => [
        'first_option' => 'Seleccionar',
        \App\User::STUDENT_TYPE => 'Estudiante',
        \App\User::TEACHER_TYPE => 'Profesor',
        \App\User::ADMIN_TYPE => 'Administrador',
        \App\User::PARENT_TYPE => 'Padre de familia',
        \App\User::COORDINATOR_TYPE => 'Coordinador',
    ],
    'show_user' => [
        'card_title' => 'Perfil de Usuario',
        'full_name' => 'Nombre completo',
        'first_name_label' => 'Nombre',
        'last_name_label' => 'Apellido',
        'local_id_label' => 'Numero de indentificación',
        'email_label' => 'Correo',
        'no_email_text' => 'Sin correo',
        'phone_label' => 'Telefono',
        'no_phone_text' => 'Sin Telefono',
        'type_label' => 'Tipo de usuario',
        'login_token_label' => 'Link de Ingreso',

        'edit_link' => 'Editar',
    ],
    'edit_user' => [
        'card_title' => 'Editar a',
        'submit_button' => 'Actualizar',
    ],
    'update_user' => [
        'success_message' => 'El usuario ha sido actualizado',
        'permission_error' => 'No tiene permisos para realizar este cambio',
    ],
    'index_users' => [
        'card_title' => 'Integrantes de ',
//        'table' => [
//            'name_header' => 'Nombre',
//            'type_header' => 'Tipo',
//        ],
        'search' => [
            'first_name' => 'Nombres',
            'last_name' => 'Apellidos',
            'local_id' => 'Identificación',
        ]
    ],
    'login_token' => [
        'welcome_title' => 'Bienvenido a Koolclass',
        'form' => [
            'submit_button' => 'Ingresar',
            'password_label' => 'Contraseña',
            'password_placeholder' => 'una contraseña que recuerdes',
            'confirm_password_label' => 'Confirmar contraseña',
            'confirm_password_placeholder' => 'de nuevo la contraseña',
        ],
        'highlight_message' => [
            'first' => 'Haz sido invitado a hacer parte de la institución ',
            'second' => 'Ahora solo debes crear una contraseña que recuerdes y podras usar Koolclass',
        ],
        'invalid_token_message' => 'Tu link de ingreso es invalido',
    ]
];
