@extends('layouts.default')

@section('content')

    @component('components.card')
        @slot('title')
            <div class="text-center">{{ __('copies.dashboard.cart_title') }}</div>
        @endslot

        <div class="p-4 flex flex-wrap justify-around">
            @if(request('message'))
                <div class="w-full">
                    <div class="p-6 my-4 rounded bg-yellow-300 border border-yellow-400 text-center">
                        <p class="border-b border-gray-600 py-2 font-bold">Bienvenido a Koolclass</p>
                        <p class="border-b-0 border-gray-600 py-2">Acá encontraras las materias, los cursos, profesores y estudaintes,  adelante elije alguno de los botones de abajo</p>
                    </div>
                    <h2 class="text-2xl font-bold text-center">Bienvenido a Koolclass</h2>
                </div>
            @endif


            <x-card-link :url="route('show-user', [auth()->id()])" :text="__('copies.dashboard.profile_link')" />
            <x-card-link :url="route('index-courses')" :text="__('copies.dashboard.courses_link')" />
            <x-card-link :url="route('index-groups')" :text="__('copies.dashboard.groups_link')" />
            <x-card-link url="/users" :text="__('copies.dashboard.users_link')" />

        </div>

    @endcomponent

@endsection
