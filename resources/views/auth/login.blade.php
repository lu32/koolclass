@extends('layouts.default')

@section('content')
<div class="w-full py-4">
    <div class="flex justify-center px-4 md:px-0">
        <div class="w-full md:w-8/12 lg:w-5/12">
            <div class="border border-gray-300 rounded">
                <div class="border-b border-gray-300 py-3 px-5 bg-gray-200 rounded-t" >{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" class="p-6">
                        @csrf

                        <div class="flex flex-wrap {{ !$errors->has('email') ? 'mb-4' : 'mb-8' }}">
                            <label for="email" class="field-label">{{ __('E-Mail Address') }}</label>

                            <div class="field-container">
                                <div class="relative">
                                    @include('components.form-alert-icon', ['field' => 'email'])
                                    <input id="email" type="email"
                                           class="field @error('email') field-error @enderror"
                                           name="email" value="{{ old('email') }}"
                                           required
                                           autocomplete="email" autofocus>
                                </div>


                                @error('email')
                                    <span class="field-error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="flex flex-wrap {{ !$errors->has('password') ? 'mb-2' : 'mb-8' }}">
                            <label for="password" class="field-label">{{ __('Password') }}</label>

                            <div class="field-container">
                                <div class="relative">
                                    @include('components.form-alert-icon', ['field' => 'password'])
                                    <input id="password" type="password"
                                           class="field @error('password') field-error @enderror"
                                           name="password" required autocomplete="current-password">
                                </div>

                                @error('password')
                                    <span class="field-error-message" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="flex mb-4">
                            <label class="field-label field-label-empty"></label>
                            <div class="md:w-8/12 md:pl-2">
                                <div class="py-2 flex items-baseline rounded focus:outline-none focus:shadow-outline w-full">
                                    <input class="mr-2" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="flex">
                            <label class="field-label field-label-empty"></label>
                            <div class="w-full md:w-8/12 md:pl-2">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="link md:ml-4" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
