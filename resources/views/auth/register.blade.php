@extends('layouts.default')

@section('content')
    @component('components.card')
        @slot('title')
            {{ __('copies.register.card_title') }}
        @endslot
        <form method="POST" action="{{ route('register') }}" class="p-6">
            @csrf

            <div class="flex flex-wrap {{ !$errors->has('first_name') ? 'mb-4' : 'mb-8' }}">
                <label for="first_name" class="field-label">{{ __('copies.register.name_label') }}</label>

                <div class="field-container">
                    <div class="relative">
                        @include('components.form-alert-icon', ['field' => 'first_name'])
                        <input id="first_name" type="text" class="field @error('first_name') field-error @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>
                    </div>
                    @error('first_name')
                    <span class="field-error-message" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="flex flex-wrap {{ !$errors->has('last_name') ? 'mb-4' : 'mb-8' }}">
                <label for="last_name" class="field-label">{{ __('copies.register.last_label') }}</label>

                <div class="field-container">
                    <div class="relative">
                        @include('components.form-alert-icon', ['field' => 'last_name'])
                        <input id="last_name" type="text" class="field @error('last_name') field-error @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>
                    </div>
                    @error('last_name')
                    <span class="field-error-message" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

             <div class="flex flex-wrap {{ !$errors->has('local_id') ? 'mb-4' : 'mb-8' }}">
                <label for="local_id" class="field-label">{{ __('copies.create_invitation.form.local_id_label') }}</label>

                <div class="field-container">
                    <div class="relative">
                        @include('components.form-alert-icon', ['field' => 'local_id'])
                        <input id="local_id" type="text" class="field @error('local_id') field-error @enderror" name="local_id" value="{{ old('local_id') }}" required autocomplete="local_id" autofocus>
                    </div>
                    @error('local_id')
                    <span class="field-error-message" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>



            <div class="flex flex-wrap {{ !$errors->has('email') ? 'mb-4' : 'mb-8' }}">
                <label for="email" class="field-label">{{ __('copies.register.email_label') }}</label>

                <div class="field-container">
                    <div class="relative">
                        @include('components.form-alert-icon', ['field' => 'email'])
                        <input id="email" type="email" class="field @error('email') field-error @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                    </div>

                    @error('email')
                    <span class="field-error-message" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="flex flex-wrap {{ !$errors->has('password') ? 'mb-4' : 'mb-8' }}">
                <label for="password" class="field-label">{{ __('copies.register.password_label') }}</label>

                <div class="field-container">
                    <div class="relative">
                        @include('components.form-alert-icon', ['field' => 'password'])
                        <input id="password" type="password" class="field @error('password') field-error @enderror" name="password" required autocomplete="new-password">
                    </div>

                    @error('password')
                    <span class="field-error-message" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="flex flex-wrap {{ !$errors->has('password_confirmation') ? 'mb-4' : 'mb-8' }}">
                <label for="password-confirm" class="field-label">{{ __('copies.register.confirm_password_label') }}</label>

                <div class="field-container">
                    <input id="password-confirm" type="password" class="field" name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>
            @include('forms.select', ['field' => 'institution_id', 'options' => $institutions, 'label' => __('copies.register.institution_selector_label')])
            <div class="flex flex-wrap mb-4">
                <label class="field-label"></label>
                <div class="field-container offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('copies.register.submit_button') }}
                    </button>
                </div>
            </div>
        </form>
    @endcomponent
@endsection
