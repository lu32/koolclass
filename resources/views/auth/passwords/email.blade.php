@extends('layouts.default')

@section('content')
    @component('components.card')
        @slot('title')
            {{ __('Reset Password') }}
        @endslot

        @if (session('status'))
            <div class="alert alert-success mx-6 mt-6" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" class="p-6" action="{{ route('password.email') }}">
            @csrf

            <div class="flex flex-wrap {{ !$errors->has('email') ? 'mb-4' : 'mb-10' }}">
                <label for="email" class="field-label">{{ __('E-Mail Address') }}</label>

                <div class="field-container">
                    <div class="relative">
                        @include('components.form-alert-icon', ['field' => 'email'])
                        <input id="email" type="email" class="field @error('email') field-error @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    </div>

                    @error('email')
                    <span class="field-error-message" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="flex flex-wrap">
                <label class="field-label field-label-empty"></label>
                <div class="field-container">
                    <button type="submit" class="btn">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>
            </div>
        </form>

    @endcomponent
@endsection
