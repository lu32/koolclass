@extends('layouts.default')

@section('content')
    @include('components.alert')
    <x-breadcrumbs deep="groupSubjects" branch="groups" :group="$group" />
    @component('components.card')
        @slot('title')
            <div class="flex content-between items-center">
                <span class="flex-grow">
                    {{ __('copies.group_courses.card_title_prefix') }} {{ $group->name }}
                </span>
            </div>
        @endslot

        <form method="POST" action="{{ route('group-courses', [$group->id]) }}" id="course-selector" class="p-4 border-b border-gray-400 flex flex-wrap">
            @csrf
            <div class="flex flex-col p-4 w-full sm:w-3/12">
                <label>{{ __('copies.group_courses.add_form.year_label') }}</label>
                <input type="text" value="{{ $group->year }}" readonly
                       class="bg-gray-300 text-center field"
                >
            </div>

            <div class="flex flex-col p-4 w-full sm:w-3/12">
                <label>{{ __('copies.group_courses.add_form.period_label') }}</label>
                <input type="text" value="{{ $group->period }}" readonly
                       class="bg-gray-300 text-center field"
                >
            </div>

            <div class="flex flex-col p-4 w-full sm:w-6/12">
                <label>{{ __('copies.group_courses.add_form.my_courses_label') }}</label>
                <div class="flex">
                    <select name="course_id"
                            id="course_id"
                            class="field bg-white @error('course_id') field-error @enderror"
                            cols="30"
                            rows="4">

                        @forelse($availableCourses as $option)
                            @if($loop->first)
                                <option value="">{{ __('copies.group_courses.add_form.first_option') }}</option>
                            @endif
                            <option value="{{ $option->id }}">{{ $option->name }}</option>
                        @empty
                            <option value="">{{ __('copies.group_courses.add_form.empty_courses_option') }}</option>
                        @endforelse
                    </select>

                </div>
            </div>

            <div class="text-center w-full">
                <button class="btn w-full" type="submit">{{ __('copies.group_courses.add_form.submit_button') }} {{ $group->name }}</button>
            </div>
        </form>

        <div class="py-6 text-center px-4">
            Si deseas crear una nueva materia <a class="link underline" href="{{ route('create-course') }}">ingresa acá</a>.
        </div>
    @endcomponent
@endsection
