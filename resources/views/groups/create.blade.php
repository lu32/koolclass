@extends('layouts.default')

@section('content')
    <x-breadcrumbs deep="indexGroups" branch="groups" />
    @component('components.card')
        @slot('title')
            {{ __('copies.create_group.card_title') }}
        @endslot
        <form method="POST" action="{{ route('store-group') }}" class="p-6">
            @csrf
            @include('forms.text-field', ['field' => 'name', 'label' => __('copies.create_group.name_label'), 'placeholder' => __('copies.create_group.name_placeholder')])
            @include('forms.select', ['field' => 'year', 'options' => $years, 'label' => __('copies.year.year_selector_label')])
            @include('forms.select', ['field' => 'period', 'options' => $periods, 'label' => __('copies.period.period_selector_label')])
            @include('forms.submit-button', ['submitText' => __('copies.create_group.submit_button')])
        </form>
    @endcomponent
@endsection
