@extends('layouts.default')

@section('content')
    @include('components.alert')
    <x-breadcrumbs deep="showGroup" :group="$group" :branch="App\View\Components\Breadcrumbs::PATH_GROUP_STUDENTS" />
    @component('components.card')
        @slot('title')
            <x-card-header>
                <x-slot name="leftButton">
                    <a onclick="addStudent.show();" href="#" class="btn">Agregar</a>
                </x-slot>

                <x-slot name="title">
                    {{ $group->name }}
                </x-slot>

                <x-slot name="right_button">
                    <a href="{{ route('edit-group', [$group->id]) }}">
                        <svg class="icon icon-edit"><use xlink:href="/assets/icons.svg#icon-edit"></use></svg>
                    </a>
                </x-slot>
            </x-card-header>

        @endslot

        <div class="p-1">
            <div id="add-student"></div>

            <livewire:group-students-list :group="$group" />

        </div>
    @endcomponent
@endsection

@push('scripts')
<script>
    var addStudent = new components.AddStudent({
        target: document.getElementById('add-student'),
        props: {
            group_id: {{ $group->id }}
        }
    })
</script>
@endpush
