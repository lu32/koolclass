@extends('layouts.default')

@section('content')
    @include('components.alert')
    <x-breadcrumbs deep="indexGroups" branch="groups" />
    @component('components.card')
        @slot('title')
            <div class="flex content-between items-center">
                <span class="flex-grow">
                    {{ $group->name }}
                </span>
                @can('update', $group)
                    <a href="{{ route('edit-group', [$group->id]) }}">
                        <svg class="icon icon-edit"><use xlink:href="/assets/icons.svg#icon-edit"></use></svg>
                    </a>
                @endcan
            </div>
        @endslot


        <div class="flex mt-2 border-0">

            <div class="w-1/2 pl-6 py-5">

                <div class="mb-2">
                    <div class="text-base leading-6 font-medium text-gray-700">Grupo</div>
                    <div class="text-base leading-6 text-gray-900">{{ $group->name }}</div>
                </div>

                <div>
                    <div class="text-base leading-6 font-medium text-gray-700">Profesor</div>
                    <div class="text-base leading-6 text-gray-900"><a class="link" href="{{ route('show-user', $group->creator_id) }}">{{ $group->creator->name() }}</a></div>
                </div>

            </div>

            <div class="w-1/2 py-5">

                <div class="mb-2">
                    <div class="text-base leading-6 font-medium text-gray-700">Año</div>
                    <div class="text-base leading-6 text-gray-900">{{ $group->year }}</div>
                </div>

                <div>
                    <div class="text-base leading-6 font-medium text-gray-700">Periodo</div>
                    <div class="text-base leading-6 text-gray-900">{{ $group->period >= 0 ? $group->periodLabel() : 'n/a' }}</div>
                </div>
            </div>

        </div>

        <div class="p-2 border-t border-gray-400">
            <h2 class="text-2xl mb-4 font-bold class flex justify-between">
                <span>Materias</span>

                <div>
                    <a class="font-normal text-base btn" href="{{ route('add-subject', [$group->id]) }}">Agregar</a>
                </div>
            </h2>
            <livewire:groups.courses :group="$group" />
        </div>

        <div class="p-2 border-t border-gray-400">
            <div id="add-student"></div>
            <h2 class="text-2xl mb-4 font-bold flex">
                <span class="flex-grow">Estudiantes</span>
                <div>
                    <a onclick="event.preventDefault(); addStudent.show();" href="#" class="btn text-base font-normal ">Agregar</a>
                </div>
            </h2>
            <livewire:group-students-list :group="$group" />
        </div>
    @endcomponent
@endsection


@push('scripts')
    <script>
        var addStudent = new components.AddStudent({
            target: document.getElementById('add-student'),
            props: {
                group_id: {{ $group->id }}
            }
        })
    </script>
@endpush
