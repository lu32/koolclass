@extends('layouts.default')

@section('content')
    <x-breadcrumbs deep="showGroup" :group="$group" branch="groups" />
    @component('components.card')
        @slot('title')
            <div class="flex content-between items-center">
                <span class="flex-grow">
                  {{ __('copies.edit_group.title_prefix') }} {{ $group->name }}
                </span>
                <a href="{{ route('show-group', [$group->id]) }}">
                    <svg class="icon"><use xlink:href="/assets/icons.svg#icon-eye"></use></svg>
                </a>
            </div>
        @endslot
        <form method="POST" action="{{ route('update-group', [$group->id]) }}" class="p-6">
            @csrf
            @method('patch')
            @include('forms.text-field', [
                            'field' => 'name',
                            'label' => __('copies.create_group.name_label'),
                            'value' => $group->name
                            ])

            @include('forms.select', [
                                'field' => 'year',
                                'options' => \App\YearsCollection::get(),
                                'selectedOption' => $group->year,
                                'label' => __('copies.year.year_selector_label')
                                ])
            @include('forms.select', [
                                    'field' => 'period',
                                    'options' => \App\PeriodsCollection::get(),
                                    'selectedOption' => $group->period,
                                    'label' => __('copies.period.period_selector_label')
                                    ])
            @include('forms.submit-button', ['submitText' => __('copies.edit_group.submit_button')])
        </form>
    @endcomponent
@endsection
