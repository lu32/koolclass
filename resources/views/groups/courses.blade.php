@extends('layouts.default')

@section('content')
    @include('components.alert')
    <x-breadcrumbs deep="showGroup" branch="groups" :group="$group" />
    @component('components.card')
        @slot('title')
            <div class="flex justify-between">
                <span class="w-1/3">
                    <a href="{{ route('add-subject', [$group->id]) }}" class="btn">
                        <svg style="margin-right: 2px;" class="inline align-baseline" height="12" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.5 27V15.5H2.41399e-05L0 11.5H11.5V0L15.5 5.18933e-06V11.5H27V15.5H15.5V27H11.5Z" fill="currentColor"/>
                         </svg>
                        Materia
                    </a>
                </span>
                <span class="w-1/3 text-center flex items-center justify-center">
                    {{ __('copies.group_courses.card_title_prefix') }} {{ $group->name }}
                </span>
                <span class="w-1/3"></span>
            </div>

        @endslot



        <div class="">
            @forelse($group->courses as $course)
                <a class="link  block pl-6 h-10 flex items-center hover:bg-blue-100
                        @if(!$loop->last) border-b border-gray-400 @endif"
                   href="{{ route('show-course-in-group', [$group->id, $course->id]) }}">
                    {{ $course->name }}
                </a>
            @empty
                <div class="p-6 m-4 rounded bg-yellow-300 border border-yellow-400 text-center">
                    <p class="border-b border-gray-600 py-2 font-bold">Asignaturas de {{ $group->name }}</p>
                    <p class="border-b border-gray-600 py-2">Acá prodras <a class="link underline" href="{{ route('add-subject', [$group->id]) }}">agregar</a> materias a grupo Sexto B</p>
                    <p class="border-b border-gray-600 py-2">De esta manera los estudiantes podrán ver todos los contenidos de estas materias</p>
                </div>
{{--                <div class="text-center p-4">{{ __('copies.group_courses.empty_courses_message') }}</div>--}}
            @endforelse
        </div>
    @endcomponent
@endsection
