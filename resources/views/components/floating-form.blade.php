<div id="floating-form-shade" class="fixed inset-0 h-full w-full bg-black opacity-50 hidden"></div>
<div id="floating-form" class="fixed inset-0 h-full w-full flex items-center justify-center hidden">
    <div class="w-full sm:w-2/3 lg:w-5/12 px-8">
        <div class="flex flex-col w-full border rounded bg-white">
            <div class="flex bg-gray-200 border-b px-4 py-4">{{ $title }}</div>
            <div class="flex px-4 py-4">{{ $slot }}</div>
        </div>
    </div>
</div>