<div class="border py-2 px-2 flex justify-between {{ isset($borderB) && $borderB ? 'border-b' : 'border-b-0' }}">
    <div>
        <div>
            <strong>Nombre:</strong> <a class="link underline" href="{{ route('show-user', $user->id) }}">{{ $user->name() }}</a>
        </div>
        <div>
            <strong>Identificación:</strong> {{ $user->local_id }}
        </div>
    </div>

    <div class="flex items-center">
        <buttton class="cursor-pointer">
            <svg class="icon icon fill-current text-red-500"><use xlink:href="/assets/icons.svg#icon-bin"></use></svg>
        </buttton>
    </div>
</div>
