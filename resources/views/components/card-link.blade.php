<div class="p-4 border-0  border-red-400 w-1/2 h-32">
    <a href="{{ $url }}"
       class="flex justify-center items-center border border-gray-700 h-full w-full rounded-md hover:bg-blue-100">
        {{ $text }}
    </a>
</div>
