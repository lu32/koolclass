<div class="{{ !$errors->has($field) ? 'hidden' : '' }} absolute right-0 top-0 bottom-0 flex px-2 flex-col justify-center">
    <svg class="text-red-600"
         xmlns='http://www.w3.org/2000/svg'
         width='22' fill='none' stroke='currentColor' viewBox='0 0 12 12'>
        <circle cx='6' cy='6' r='4.5'/>
        <path stroke-linejoin='round' d='M5.8 3.6h.4L6 6.5z'/>
        <circle cx='6' cy='8.2' r='.6' fill='currentColor' stroke='none'/>
    </svg>
</div>
