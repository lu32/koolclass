@if(session()->has('success'))
    <div class="alert alert-success w-11/12 md:w-8/12 lg:w-5/12 mx-auto my-4" role="alert">
        {{ session('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert bg-red-200 border-red-300 text-red-900 w-11/12 md:w-8/12 lg:w-5/12 mx-auto my-4" role="alert">
        {{ session('error') }}
    </div>
@endif

@if ($errors->any())
    <div class="alert bg-red-200 border-red-300 text-red-900">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
