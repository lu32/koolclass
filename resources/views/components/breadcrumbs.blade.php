<div class="flex justify-center sm:px-0">
    <div class="w-full sm:w-7/12 lg:w-4/12 border border-b-0 border-gray-300 p-2 rounded-t text-center shadow-xl">
        @foreach($links as $link)
            @if($link)
                <a class="link" href="{{ $link['url'] }}">{{ $link['text'] }}</a>
                @if(!$loop->last) > @endif
            @endif
        @endforeach
    </div>
</div>
