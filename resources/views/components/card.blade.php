<div class="flex justify-center sm:px-0">
    <div class="w-full sm:w-8/12 border-t border-b sm:border  border-gray-400 sm:rounded" style="max-width: 710px;">
        <div class="border-b border-gray-400 py-3 px-5 bg-white sm:rounded-t" >{{ $title }}</div>
        <div class="card-body">
            {{ $slot }}
        </div>
    </div>
</div>
