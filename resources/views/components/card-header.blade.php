<div class="flex flex-wrap flex-col-reverse md:flex-row justify-between">
    <div class="w-full md:w-1/3 text-center md:text-left md:flex md:items-center">
        {{ $leftButton ?? '' }}
    </div>
    <div class="w-full md:w-1/3 text-center flex items-center justify-center text-gray-800 text-2xl md:flex md:items-center">
        {{ $title }}
    </div>
    <div class="w-full md:w-1/3 text-center md:flex md:justify-end items-center">
        {{ $right_button ?? '' }}
    </div>
</div>
