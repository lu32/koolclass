@extends('layouts.default')

@section('content')
    <div class="mt-16">
        <div class="px-6 text-left md:text-center xl:text-left max-w-2xl md:max-w-3xl mx-auto">
            <h1 class="text-3xl sm:text-4xl md:text-5xl xl:text-4xl font-light leading-tight">{{ __('copies.home.welcome_title.first_part') }} <span class="sm:block text-teal-500 font-normal">{{ __('copies.home.welcome_title.second_part') }}.</span></h1>
            <p class="mt-6 leading-relaxed sm:text-lg md:text-xl xl:text-lg text-gray-600">
                {{ __('copies.home.welcome_message') }}
            </p>
            <div class="flex mt-6 justify-start md:justify-center xl:justify-start">
                <a href="{{ route('register') }}" class="rounded-lg px-4 md:px-5 xl:px-4 py-3 md:py-4 xl:py-3 bg-teal-500 hover:bg-teal-600 md:text-lg xl:text-base text-white font-semibold leading-tight shadow-md">Empezar</a>
                <a href="#what-is-tailwind" class="ml-4 rounded-lg px-4 md:px-5 xl:px-4 py-3 md:py-4 xl:py-3 bg-white hover:bg-gray-200 md:text-lg xl:text-base text-gray-800 font-semibold leading-tight shadow-md">Por que Koolclas?</a>
            </div>
        </div>
    </div>
@endsection
