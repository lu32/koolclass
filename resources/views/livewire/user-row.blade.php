<div class="border py-2 pr-2 flex flex-wrap justify-between
            {{ isset($borderB) && $borderB ? 'border-b' : 'border-b-0' }}
            {{ $removed ? 'hidden': '' }}
    ">
    <div class="w-11/12 flex">
        <div class="flex items-center px-2">
            {{ $iteration }}
        </div>
        <div class="flex-grow">
            <div>
                <strong>Nombres:</strong> <a class="link underline" href="{{ route('show-user', $user->id) }}">{{ $user->first_name }}</a>
            </div>
            <div>
                <strong>Apellidos:</strong> <a class="link underline" href="{{ route('show-user', $user->id) }}">{{ $user->last_name }}</a>
            </div>
            <div>
                <strong>Identificación:</strong> {{ $user->local_id }}
            </div>
        </div>
    </div>

    @if($group)
        <div class="flex items-center justify-center w-1/12 border">

            <buttton class="cursor-pointer" wire:click="showRemoveWarning" wire:loading.class="hidden">
                <svg class="icon icon fill-current text-red-500"><use xlink:href="/assets/icons.svg#icon-bin"></use></svg>
            </buttton>
            <div class="btn-spin relative border w-8 h-8 hidden" wire:loading.class.remove="hidden" wire:target="showRemoveWarning"></div>
        </div>
    @endif

    @if($showRemoveWarning && $group)
        <div class="ml-2 bg-red-200 mt-2 border
                    border-red-400 rounded py-2 px-2
                    text-red-800 flex flex-grow flex-wrap
                    ">
            <div class="flex-grow flex items-center">
                <p>Seguro desea quitar a {{ $user->first_name }} de este curso?</p>
            </div>
            <div class="flex items-center">
                <button class="btn h-8 mr-2" wire:loading.class="btn-spin" wire:click="removeStudent">Si</button>
                <button class="btn h-8 bg-gray-300 text-gray-800" wire:loading.class="btn-spin" wire:click="hideRemoveWarning">No</button>
            </div>
        </div>
    @endif

</div>

