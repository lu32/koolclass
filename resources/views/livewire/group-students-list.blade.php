<div>
    @forelse($students as $student)
        @livewire('user-row', [
                                'user' => $student,
                                'group' => $group,
                                'borderB' => $loop->last,
                                'iteration' => $loop->iteration,
                                    ],
                                 key($student->id))
    @empty
        <div class="p-6 my-4 rounded bg-yellow-300 border border-yellow-400 text-center">
            No se han agregado estudiantes a esta clase.

            <div>
                <button class="btn" onclick="addStudent.show();">Agregar</button>
            </div>
        </div>
    @endforelse

</div>
