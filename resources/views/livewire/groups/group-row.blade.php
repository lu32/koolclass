
<div class="py-4 md:py-2 flex flex-wrap items-center hover:bg-blue-100
                        @if(!$isLast) border-b border-gray-400 @endif"
>

    <a class="pl-2 py-1 link w-full md:w-1/4" href="{{ route('show-group', [$group->id]) }}">
        <strong class="md:hidden text-black">{{ __('copies.show_course.list_headers.course_name') }}: </strong>
        {{ $group->name }}
    </a>
    <a class="pl-2 py-1 link w-full md:w-1/4" href="{{ route('show-user', $group->creator_id) }}">
        <strong class="md:hidden text-black">{{ __('copies.show_course.list_headers.creator') }}: </strong>
        {{ $group->creator->name() }}
    </a>
    <div class="pl-2 py-1 w-full md:w-1/4">
        <strong class="md:hidden text-black">{{ __('copies.show_course.list_headers.year') }}: </strong>
        {{ $group->year }}
    </div>
    <div class="pl-2 py-1 w-full md:w-1/4">
        <strong class="md:hidden text-black">{{ __('copies.show_course.list_headers.period') }}: </strong>
        {{ $group->periodLabel() }}
    </div>


</div>
