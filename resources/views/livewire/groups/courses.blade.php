<div>
    @forelse($group->courses as $course)
        <a class="link  block pl-6 h-10 flex items-center hover:bg-blue-100
                    @if(!$loop->last) border-b border-gray-400 @endif"
           href="{{ route('show-course', [$course->id]) }}">
            {{ $course->name }}
        </a>
    @empty
        <div class="p-6 m-4 rounded bg-yellow-300 border border-yellow-400 text-center">
            <p class="border-b border-gray-600 py-2 font-bold">Asignaturas de {{ $group->name }}</p>
            <p class="border-b border-gray-600 py-2">Acá prodras <a class="link underline" href="{{ route('add-subject', [$group->id]) }}">agregar</a> materias a {{ $group->name }}</p>
            <p class="border-b border-gray-600 py-2">De esta manera los estudiantes podrán ver todos los contenidos de estas materias</p>
        </div>
    @endforelse
</div>
