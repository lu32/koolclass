<div class="p-4 justify-center">

    <form action="" method="get" class="flex flex-wrap py-4">
        <div class="flex-grow px-4 w-full md:w-1/4 mb-3">
            <label class="block font-bold">{{ __('copies.index_users.search.first_name') }}</label>
            <input  wire:model.debounce.400ms="first_name" class="border h-8 border-gray-700 w-full bg-white rounded" >
        </div>

        <div class="flex-grow px-4 w-full md:w-1/4 mb-3">
            <label class="block font-bold">{{ __('copies.index_users.search.last_name') }}</label>
            <input  wire:model.debounce.400ms="last_name" class="border h-8 border-gray-700 w-full bg-white rounded" >
        </div>

        <div class="flex-grow px-4 w-full md:w-1/4 mb-3">
            <label class="block font-bold">{{ __('copies.index_users.search.local_id') }}</label>
            <input  wire:model.debounce.400ms="local_id" class="border h-8 border-gray-700 w-full bg-white rounded" >
        </div>


        <div class="flex-grow px-4 w-full md:w-1/4">
            <label class="block font-bold">&nbsp;</label>
            <button class="btn w-full" type="button" wire:loading.class="btn-spin" wire:click="$refresh">Filtrar</button>
        </div>

    </form>

    <div>
        <div class="border-0 py-2 border-red-300">
            {{ $users->onEachSide(2)->links('vendor.pagination.tailwind') }}
        </div>
        @forelse($users as $user)
            <livewire:user-row :user="$user"
                               :border-b="$loop->last"
                               :iteration="$loop->iteration + $users->firstItem() - 1"
                                :key="$user->id"/>
        @empty

        @endforelse

        <div class="border-0 py-2 border-red-300">
            {{ $users->onEachSide(2)->links('vendor.pagination.tailwind') }}
        </div>
    </div>

</div>
