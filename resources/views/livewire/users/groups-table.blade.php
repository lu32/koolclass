<div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
    <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
        <table class="min-w-full border-0">
            <thead>
            <tr>
                <th class="px-6 py-3 border-0 border-b
                                   border-gray-200 bg-gray-100 text-left
                                   text-xs leading-4 font-medium text-gray-500
                                   uppercase tracking-wider">Materia</th>
                <th class="px-6 py-3 border-0 border-b
                                   border-gray-200 bg-gray-100 text-left
                                   text-xs leading-4 font-medium text-gray-500
                                   uppercase tracking-wider">Año</th>
                <th class="px-6 py-3 border-0 border-b
                                   border-gray-200 bg-gray-100 text-left
                                   text-xs leading-4 font-medium text-gray-500
                                   uppercase tracking-wider">Periodo</th>
                <th class="px-6 py-3 border-0 border-b
                                   border-gray-200 bg-gray-100 text-left
                                   text-xs leading-4 font-medium text-gray-500
                                   uppercase tracking-wider">Profesor</th>
                <th class="px-6 py-3 border-0 border-b
                                   border-gray-200 bg-gray-100 text-left
                                   text-xs leading-4 font-medium text-gray-500
                                   uppercase tracking-wider">Materias</th>
            </tr>
            </thead>
            <tbody class="bg-white">
            @foreach($groups as $group)
                <tr>
                    <td class="px-6 py-4 whitespace-no-wrap @if(!$loop->last) border-b @endif border-gray-200">
                        <a class="link underline" href="{{ route('show-group', $group->id) }}">{{ $group->name }}</a>
                    </td>
                    <td class="px-6 py-4 whitespace-no-wrap @if(!$loop->last) border-b @endif border-gray-200">{{ $group->year }}</td>
                    <td class="px-6 py-4 whitespace-no-wrap @if(!$loop->last) border-b @endif  border-gray-200">{{ $group->periodLabel() }}</td>
                    <td class="px-6 py-4 whitespace-no-wrap @if(!$loop->last) border-b @endif  border-gray-200">
                        <a class="link underline" href="{{ route('show-user', $group->creator->id) }}">{{ $group->creator->name() }}</a>
                    </td>
                    <td class="px-6 py-4 whitespace-no-wrap @if(!$loop->last) border-b @endif  border-gray-200">
                        @foreach($group->courses as $course)
                            <a class="link block" href="{{ route('show-course', $course->id) }}">{{ $course->name }}</a>
                        @endforeach
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>
