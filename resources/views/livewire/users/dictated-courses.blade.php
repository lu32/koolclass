<div>
    <div class="border-b border-t border-gray-400 py-3 px-5 bg-white text-center text-xl" >Grupos dirigidos</div>

    @if($user->createdGroups->count())
        <div class="flex flex-col p-4">
            <livewire:users.groups-table :groups="$user->createdGroups" />
        </div>
    @else
        <div class="text-center border border-yellow-400 m-4 rounded bg-yellow-300 py-5">El profesor no esta dirigiendo ningun grupo</div>
    @endif
</div>
