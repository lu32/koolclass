<div>
    @if($user->canTeach())
        <div class="border-b border-t border-gray-400 py-3 px-5 bg-white text-center text-xl" >Materias dictadas</div>
        @if($user->courses->count())
            <div class="flex flex-col p-4">
                <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
                    <div class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200">
                        <table class="min-w-full border-0">
                            <thead>
                            <tr>
                                <th class="px-6 py-3 border-0 border-b
                                   border-gray-200 bg-gray-100 text-left
                                   text-xs leading-4 font-medium text-gray-500
                                   uppercase tracking-wider">Materia</th>
                                <th class="px-6 py-3 border-0 border-b
                                   border-gray-200 bg-gray-100 text-left
                                   text-xs leading-4 font-medium text-gray-500
                                   uppercase tracking-wider">Año</th>
                                <th class="px-6 py-3 border-0 border-b
                                   border-gray-200 bg-gray-100 text-left
                                   text-xs leading-4 font-medium text-gray-500
                                   uppercase tracking-wider">Periodo</th>
                            </tr>
                            </thead>
                            <tbody class="bg-white">
                            @foreach($user->courses as $course)
                                <tr>
                                    <td class="px-6 py-4 whitespace-no-wrap @if(!$loop->last) border-b @endif border-gray-200">
                                        <a class="link underline" href="{{ route('show-course', $course->id) }}">{{ $course->name }}</a>
                                    </td>
                                    <td class="px-6 py-4 whitespace-no-wrap @if(!$loop->last) border-b @endif border-gray-200">{{ $course->year }}</td>
                                    <td class="px-6 py-4 whitespace-no-wrap @if(!$loop->last) border-b @endif  border-gray-200">{{ $course->periodLabel() }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="text-center border border-yellow-400 m-4 rounded bg-yellow-300 py-5">El usuario no esta dictando cursos en la actualidad</div>
        @endif
    @endif
</div>
