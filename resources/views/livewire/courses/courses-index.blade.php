
<div>
    <x-breadcrumbs deep="dashboard" branch="courses" />
    @component('components.card')
        @slot('title')
            <x-card-header>
                @can('create', \App\Course::class)
                <x-slot name="leftButton">
                    <a role="create-course-button" href="{{ route('create-course') }}" class="btn">{{ __('copies.index_courses.new_button') }}</a>
                </x-slot>
                @endcan
                <x-slot name="title">
                    {{ __('copies.index_courses.card_title') }}
                </x-slot>
            </x-card-header>
        @endslot

        <div>
                <form action="" method="get" class="flex flex-wrap py-4">
                    <div class="flex-grow px-4 w-full md:w-1/4 mb-3">
                        <label class="block font-bold">{{ __('copies.show_course.filters.year_label') }}</label>
                        <select wire:model="year" wire:change="filter" class="border h-8 border-gray-700 w-full bg-white" name="year" >
                            @foreach(\App\YearsCollection::get() as $yearOption)
                                <option value="{{ $yearOption->id }}">{{ $yearOption->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="flex-grow px-4 w-full md:w-1/4 mb-3">
                        <label class="block font-bold">{{ __('copies.show_course.filters.period_label') }}</label>
                        <select wire:model="period" wire:change="filter" class="border h-8 border-gray-700 w-full bg-white" name="period">
                            @foreach(\App\PeriodsCollection::get() as $periodOption)
                                <option value="{{ $periodOption->id }}">{{ $periodOption->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="flex-grow px-4 w-full md:w-1/4">
                        <label class="block font-bold">{{ __('copies.show_course.filters.teacher_label') }}</label>
                        <select wire:model="creator_id" wire:change="filter" class="border h-8 border-gray-700 w-full bg-white" name="teacher_id" >
                            <option value="">Seleccionar</option>
                            @foreach($teachers as $teacher)
                                <option value="{{ $teacher->id }}">{{ $teacher->name() }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="flex-grow px-4 w-full md:w-1/4">
                        <label class="block font-bold">&nbsp;</label>
                        <button class="btn w-full" type="button" wire:loading.class="btn-spin" wire:click="filter" wire:target="filter">Filtrar</button>
                    </div>

                </form>

                @forelse($courses as $course)
                    @if($loop->first)
                        <div class="hidden font-bold h-10 md:flex items-center hover:bg-blue-100
                     border-b border-t border-gray-400">
                            <div class="pl-2 w-1/4">{{ __('copies.show_course.list_headers.course_name') }}</div>
                            <div class="w-1/4">{{ __('copies.show_course.list_headers.creator') }}</div>
                            <div class="w-1/4">{{ __('copies.show_course.list_headers.year') }}</div>
                            <div class="w-1/4">{{ __('copies.show_course.list_headers.period') }}</div>
                        </div>
                        <livewire:courses.course-row :is-last="$loop->last" :course="$course" :key="$course->id"></livewire:courses.course-row>
                    @else
                        <livewire:courses.course-row :is-last="$loop->last" :course="$course" :key="$course->id"></livewire:courses.course-row>
                    @endif
                @empty
                    @if($this->areFiltersEmpty)
                        <div class="p-6 m-4 rounded bg-yellow-300 border border-yellow-400 text-center">
                            <p class="border-b border-gray-600 py-2 font-bold">Lista de materias</p>
                            <p class="border-b border-gray-600 py-2">Aca prodras <a class="link underline" href="{{ route('create-course') }}">crear</a> nuevas materias</p>
                            <p class="border-b border-gray-600 py-2">Ver lista de cursos que se están dictando</p>
                        </div>
                    @else
                        <div class="p-6 m-4 rounded bg-yellow-300 border border-yellow-400 text-center">
                            <p class="border-b border-gray-600 py-2 text-xl">
                                No se encontraron materias para los filtros seleccionados
                            </p>
                        </div>
                    @endif
                @endforelse

            </div>

    @endcomponent

</div>


