<div>
    @include('components.alert')
    <div>
        <x-breadcrumbs deep="showCourse" :course="$course" branch="courses" />
        @component('components.card')
            @slot('title')
                <div class="flex content-between items-center">
                    <div class="w-1/3 relative">
                        <button id="dropdown-button" class="btn" onclick="toggleActionsDropdown()">{{ __('copies.course_files.upload_button') }}</button>
                        <div id="actions-dropdown" class="hidden border border-blue-400 rounded w-48 bg-white absolute">
                            <a class="block h-10 flex items-center pl-3 hover:bg-blue-200" onclick="event.preventDefault(); fileUploader.open()" href="#">
                                <svg class="icon mr-3"><use xlink:href="/assets/icons.svg#icon-upload"></use></svg> Subir archivo
                            </a>
                            <a class="block h-10 flex items-center pl-3 hover:bg-blue-200" href="#" onclick="event.preventDefault(); toggleFolderCreationForm()">
                                <svg class="icon mr-3"><use xlink:href="/assets/icons.svg#icon-folder"></use></svg> Crear carpeta
                            </a>
                        </div>
                    </div>
                    <span class="text-center w-1/3">
                  {{ __('copies.course_files.card_title') }}
                </span>
                </div>
            @endslot


            <div style="min-height: 450px">
                <div id="uploading-files"></div>
                @if(!$folder->uploads->count() && !$folder->folders->count())
                    <div class="p-4">
                        <div class="p-4 mb-4 rounded bg-yellow-300 border border-yellow-400 text-center text-xl">
                            {{ __('copies.course_files.empty_message.first_part') }}
                            <a href="#" class="link underline" onclick="event.preventDefault(); fileUploader.open()">{{ __('copies.course_files.empty_message.second_part') }}</a>
                            {{ __('copies.course_files.empty_message.third_part') }}.
                        </div>
                    </div>
                @else
                    <div class="flex flex-wrap">
                        @if($folder->folders->count())
                            @foreach($folder->folders as $childFolder)
                                <div class="w-1/2 sm:w-1/3 flex justify-center p-4">
                                    <a href="./{{ $childFolder->id }}" class="h-48 w-32 text-center flex flex-col">
                                        <div class="flex-grow-0 w-full">
                                            <svg class="text-blue-500 flex w-full h-20 fill-current"><use xlink:href="/assets/icons.svg#icon-folder"></use></svg>
                                        </div>
                                        <div class="flex justify-center w-full">{{ $childFolder->name }}</div>
                                    </a>
                                </div>
                            @endforeach
                        @endif

                        @if($folder->uploads->count())
                            @foreach($folder->uploads as $upload)
                                <div class="w-1/2 sm:w-1/3 flex justify-center p-4">
                                    <div class="h-48 w-32 border rounded text-center">
                                        <a href="{{ $tempUrl = Storage::disk('s3')
                                                ->temporaryUrl(
                                                $upload->path,
                                                now()->addMinutes(10),
                                                ['ResponseContentDisposition' => "filename=\"{$upload->name}\""]) }}"
                                           class="h-full w-full flex flex-col h-full w-full"
                                        >
                                            <div class="border-b h-24 overflow-hidden">
                                                @if($upload->isImage())
                                                    <img src="{{ $tempUrl }}" alt="{{ $upload->name }}">
                                                @endif
                                            </div>
                                            <div class="px-2 text-sm flex items-center justify-center flex-grow break-words break-all">
                                                <span>{{ $upload->name }}</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                @endif
            </div>
            @component('components.floating-form')
                @slot('title')
                    Creación de carpeta
                @endslot
                <form method="POST" action="{{ route('store-folder', [$folder->id]) }}">
                    @csrf
                    @include('forms.text-field', ['field' => 'name', 'label' => __('copies.create_folder.name_label')])
                    <div class="flex flex-wrap mb-4">
                        <label class="field-label"></label>
                        <div class="field-container offset-md-4">
                            <button type="submit" class="btn btn-primary">{{__('copies.create_folder.submit_button')}}</button>
                            <button type="button" class="text-blue-500 underline font-semibold ml-4" onclick="toggleFolderCreationForm()">{{__('copies.create_folder.cancel_button')}}</button>
                        </div>
                    </div>
                </form>
            @endcomponent
        @endcomponent
    </div>
</div>


@push('scripts')
    <script>
        var fileUploader = new components.Uploader({
            target: document.getElementById('uploading-files'),
            props: {
                folder_id: {!! $folder->id !!}
            }
        });

        const toggleActionsDropdown = () => {
            let actionsDropdown = document.getElementById('actions-dropdown');
            actionsDropdown.classList.toggle("hidden");
        }

        window.onclick = function(event) {
            let actionsDropdown = document.getElementById('actions-dropdown');
            let dropdownButton = document.getElementById('dropdown-button');
            if(!dropdownButton.contains(event.target)){
                actionsDropdown.classList.add("hidden");
            }
        }

        const toggleFolderCreationForm = () => {
            let folderCreationForm = document.getElementById('floating-form');
            let folderCreationFormShade = document.getElementById('floating-form-shade');
            folderCreationForm.classList.toggle("hidden");
            folderCreationFormShade.classList.toggle("hidden");
            document.getElementById('name').focus();
            document.getElementById('name').select();
        }
    </script>
@endpush
