<div>
    <x-breadcrumbs deep="showCourse" :course="$course" branch="courses" />
    @component('components.card')
        @slot('title')
            <x-card-header>

                <x-slot name="leftButton">
                    <a role="create-course-button" href="{{ route('create-message', $course->id) }}" class="btn">{{ __('copies.course_messages.create_link') }}</a>
                </x-slot>
                <x-slot name="title">
                    {{ __('copies.course_messages.card_title') }}
                </x-slot>
            </x-card-header>
        @endslot

        <div class="p-4">
            @foreach ($course->messages()->latest()->get() as $message)
                <a href="#" class="block border-b border-gray-600 pl-1 py-3 hover:bg-gray-200 cursor-pointer">
                    <h2 class="font-bold text-2xl">{{ $message->title }}</h2>
                    {!! $message->body !!}
                </a>
            @endforeach
        </div>
    @endcomponent
</div>
