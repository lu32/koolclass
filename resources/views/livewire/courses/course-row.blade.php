<div class="py-4 md:py-2 flex flex-wrap items-center hover:bg-blue-100
                        @if(!$isLast) border-b border-gray-400 @endif"
  >

    <a class="pl-2 py-1 link w-full md:w-1/4" href="{{ route('show-course', [$course->id]) }}">
        <strong class="md:hidden text-black">{{ __('copies.show_course.list_headers.course_name') }}: </strong>
        {{ $course->name }}
    </a>
    <a class="pl-2 py-1 link w-full md:w-1/4" href="{{ route('show-user', $course->creator_id) }}">
        <strong class="md:hidden text-black">{{ __('copies.show_course.list_headers.creator') }}: </strong>
        {{ $course->creator->name() }}
    </a>
    <div class="pl-2 py-1 w-full md:w-1/4">
        <strong class="md:hidden text-black">{{ __('copies.show_course.list_headers.year') }}: </strong>
        {{ $course->year }}
    </div>
    <div class="pl-2 py-1 w-full md:w-1/4">
        <strong class="md:hidden text-black">{{ __('copies.show_course.list_headers.period') }}: </strong>
        {{ $course->periodLabel() }}
    </div>


</div>
