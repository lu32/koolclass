<div>
    <x-breadcrumbs :course="$course" :branch="\App\View\Components\Breadcrumbs::PATH_COURSES" deep="messages" />

    <x-card>
        <x-slot name="title">
            Nuevo mensaje
        </x-slot>

        <div class="p-4">
            <input
                    wire:model="title"
                    class="border-0 outline-none border-gray-600 rounded h-16 text-3xl py-2 w-full"
                    type="text"
                    placeholder="Escribe un titulo"
            >
        </div>

        <div class="p-4">
            <x-input.rich-text wire:model.lazy="body" :initial-value="$body" />
        </div>

        <div class="p-4">
            <button class="btn"
                    wire:loading.class="btn-spin"
                    wire:click="save">{{ __('copies.create_message.save_button') }}</button>
        </div>


    </x-card>

</div>
