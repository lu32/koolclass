@extends('layouts.default')

@section('content')
    <x-breadcrumbs deep="indexCourses" branch="courses" />
    @component('components.card')
        @slot('title')
            {{ __('copies.create_course.card_title') }}
        @endslot
        <form method="POST" action="{{ route('store-course') }}" class="p-6">
            @csrf
            <div class="p-4 mb-4 rounded bg-yellow-300 border border-yellow-400 text-center">
                <p class="py-2">Aca prodras crear una nueva materia, ej. Matematicas, español, Biologia, etc.</p>
            </div>
            @include('forms.text-field', ['field' => 'name', 'label' => __('copies.create_course.name_label'), 'placeholder' => __('copies.create_course.name_placeholder')])
            @include('forms.select', ['field' => 'year', 'options' => $years, 'label' => __('copies.year.year_selector_label')])
            @include('forms.select', ['field' => 'period', 'options' => $periods, 'label' => __('copies.period.period_selector_label')])
            @include('forms.submit-button', ['submitText' => __('copies.create_course.submit_button')])
        </form>
    @endcomponent

@endsection
