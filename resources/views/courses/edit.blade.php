@extends('layouts.default')

@section('content')
    <x-breadcrumbs deep="showCourse" :course="$course" branch="courses" />
    @component('components.card')
        @slot('title')
            <div class="flex content-between items-center">
                <span class="flex-grow">
                  {{ __('copies.edit_course.title_prefix') }} {{ $course->name }}
                </span>
                <a href="{{ route('show-course', [$course->id]) }}">
                    <svg class="icon"><use xlink:href="/assets/icons.svg#icon-eye"></use></svg>
                </a>
            </div>
        @endslot
        <form method="POST" action="{{ route('update-course', [$course->id]) }}" class="p-6">
            @csrf
            @method('patch')
            @include('forms.text-field', [
                            'field' => 'name',
                            'label' => __('copies.create_course.name_label'),
                            'value' => $course->name
                            ])
            @include('forms.submit-button', ['submitText' => __('copies.edit_course.submit_button')])
        </form>
    @endcomponent

@endsection
