@extends('layouts.default')

@section('content')
    @include('components.alert')
    @isset($group)
        <x-breadcrumbs deep="indexCourses" :group="$group" :branch="\App\View\Components\Breadcrumbs::PATH_GROUP_COURSES" />
    @else
        <x-breadcrumbs deep="indexCourses" branch="courses" />
    @endisset

    @component('components.card')
        @slot('title')
            <div class="flex content-between items-center">
                <span class="flex-grow font-medium text-2xl">
                    {{ $course->name }}
                </span>
                <a href="{{ route('edit-course', [$course->id]) }}">
                    <svg class="icon icon-edit">
                        <use xlink:href="/assets/icons.svg#icon-edit"></use>
                    </svg>
                </a>
            </div>
        @endslot

        <div class="flex p-4">
            <x-card-link :url="route('course-files', [$course->id, $course->getMainFolder()->id])" :text="__('copies.show_course.files_card_title')" />
            <x-card-link :url="route('course-messages', [$course->id])" text="Mensajes" />
        </div>

        <div class="border-gray-400 border-t">

            @forelse($course->groups as $group)
                @if($loop->first)
                    <h2 class="font-medium text-xl pl-4 py-3 border-b border-gray-400">Grupos que ven esta materia</h2>
                @endif
                <a class="link block pl-6 h-10 flex items-center hover:bg-blue-100
                        @if(!$loop->last) border-b border-gray-400 @endif"
                   href="{{ route('show-group', [$group->id]) }}">
                    {{ $group->name }}
                </a>
            @empty
                <div class="p-6 m-4 rounded bg-yellow-300 border border-yellow-400 text-center">
                    <p class="border-b-0 border-gray-600 py-2 font-bold">Ningun grupo de estudiantes tiene acceso a esta materia</p>
                </div>
            @endforelse
        </div>
    @endcomponent
@endsection
