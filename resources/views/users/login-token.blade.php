@extends('layouts.default')

@section('content')
    @if($user)
        <h1 class="text-center font-light">
            <div class="text-4xl">
                {{ __('copies.login_token.welcome_title') }}
            </div>
            <div class="text-6xl text-teal-500">
                {{ $user->first_name }}
            </div>
        </h1>
    @endif

    <section class="flex justify-center px-2 md:p-0 border-0 border-red-300">
        @if(!$user)
            <div class="p-6 my-4 rounded bg-yellow-300 border border-yellow-400 text-center">
                <p class="border-b-0 border-gray-600 py-2 font-bold">{{ __('copies.login_token.invalid_token_message') }}</p>
            </div>
        @else
            <form action="{{ route('login-token', [$user->login_token]) }}" method="POST" class="w-full md:w-1/2 border border-gray-400 p-4 rounded">
                @csrf
                <div class="p-6 my-4 rounded bg-yellow-300 border border-yellow-400 text-center">
                    <p class="border-b border-gray-600 py-2 font-bold">{{ __('copies.login_token.highlight_message.first') }} {{ $user->institution->name }}</p>
                    <p class="border-b-0 border-gray-600 py-2">{{ __('copies.login_token.highlight_message.second') }}</p>
                </div>

                @include('forms.text-field', [
                            'field' => 'password',
                            'label' => __('copies.login_token.form.password_label'),
                            'placeholder' => __('copies.login_token.form.password_placeholder'),
                            'type' => 'password'
                            ])

                @include('forms.text-field', [
                           'field' => 'password_confirmation',
                           'label' => __('copies.login_token.form.confirm_password_label'),
                           'placeholder' => __('copies.login_token.form.confirm_password_placeholder'),
                           'type' => 'password'
                           ])

                <div class="flex flex-wrap mb-4">
                    <label class="field-label"></label>
                    <div class="field-container offset-md-4">
                        <button type="submit" class="py-3 btn btn-primary w-full text-2xl font-thin">{{ __('copies.login_token.form.submit_button') }}</button>
                    </div>
                </div>

            </form>
        @endif


    </section>
@endsection
