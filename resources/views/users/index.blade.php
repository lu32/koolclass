@extends('layouts.default')

@section('content')

    <x-breadcrumbs :branch="\App\View\Components\Breadcrumbs::PATH_USERS" deep="dashboard" />

    @component('components.card')
        @slot('title')
            <x-card-header>
                <x-slot name="leftButton">
                    <a href="/users/invite" class="btn">Invitar</a>
                </x-slot>
                <x-slot name="title">
                    {{ __('copies.index_users.card_title') }} {{ auth()->user()->institution->name }}
                </x-slot>
            </x-card-header>
        @endslot

        <livewire:users.index />

    @endcomponent

@endsection
