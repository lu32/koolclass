@extends('layouts.default')

@section('content')
    <x-breadcrumbs :branch="\App\View\Components\Breadcrumbs::PATH_USERS" deep="showUser" :user="$user" />
    @component('components.card')
        @slot('title')
            <x-card-header>
                <x-slot name="title">
                    {{ __('copies.edit_user.card_title') }} {{ $user->first_name }}
                </x-slot>
            </x-card-header>
        @endslot

        <form action="{{ route('update-user', [$user->id]) }}" method="POST" class="p-4">
            @method('PATCH')
            @csrf
            <div class="mb-4 border-b border-gray-400 pb-0">
                @include('forms.text-field', ['field' => 'first_name', 'label' => __('copies.show_user.first_name_label'), 'value' => $user->first_name])
            </div>

            <div class="mb-4 border-b border-gray-400 pb-0">
                @include('forms.text-field', ['field' => 'last_name', 'label' => __('copies.show_user.last_name_label'), 'value' => $user->last_name])
            </div>

            <div class="mb-4 border-b border-gray-400 pb-0">
                @include('forms.text-field', ['field' => 'local_id', 'label' => __('copies.show_user.local_id_label'), 'value' => $user->local_id])
            </div>

            <div class="mb-4 border-b border-gray-400 pb-0">
                @include('forms.text-field', ['field' => 'email', 'label' => __('copies.show_user.email_label'), 'value' => $user->email])
            </div>

            <div class="mb-4 border-b border-gray-400 pb-0">
                @include('forms.text-field', ['field' => 'phone', 'label' => __('copies.show_user.phone_label'), 'value' => $user->phone, 'placeholder' => __('copies.create_invitation.form.phone_placeholder')])
            </div>

            <div class="mb-4 border-b border-gray-400 pb-0">
                @include('forms.select', [
                'label' => 'Tipo',
                'field' => 'type',
                'selectedOption' => $user->type,
                'options' => \App\User::dropdownOptions()
            ])
            </div>

            @include('forms.submit-button', ['submitText' => __('copies.edit_user.submit_button')])

        </form>


    @endcomponent

@endsection
