@extends('layouts.default')

@section('content')
    @component('components.card')
        @slot('title')
            <x-card-header>
                <x-slot name="leftButton">
                    <a href="/users" class="link">{{ __('copies.create_invitation.card_title') }}</a>
                </x-slot>
                <x-slot name="title">
                    {{ __('copies.create_invitation.card_title') }}
                </x-slot>
            </x-card-header>
        @endslot

        <form method="POST" action="{{ route('store-user') }}" class="p-6">
           <div class="pb-4">
               @include('components.alert')
           </div>

            @csrf
            @include('forms.text-field', [
                                            'field' => 'first_name',
                                            'label' => __('copies.create_invitation.form.first_name_label'),
                                            'placeholder' => __('copies.create_invitation.form.first_name_placeholder')
                                            ])
            @include('forms.text-field', [
                                            'field' => 'last_name',
                                            'label' => __('copies.create_invitation.form.last_name_label'),
                                            'placeholder' => __('copies.create_invitation.form.last_name_placeholder')
                                            ])

            @include('forms.text-field', [
                                            'type' => 'text',
                                            'field' => 'local_id',
                                            'label' => __('copies.create_invitation.form.local_id_label'),
                                            'placeholder' => __('copies.create_invitation.form.local_id_placeholder')
                                            ])

            @include('forms.text-field', [
                                            'type' => 'email',
                                            'field' => 'email',
                                            'label' => __('copies.create_invitation.form.email_label'),
                                            'placeholder' => __('copies.create_invitation.form.email_placeholder')
                                            ])

            @include('forms.text-field', [
                                           'type' => 'number',
                                           'field' => 'phone',
                                           'label' => __('copies.create_invitation.form.phone_label'),
                                           'placeholder' => __('copies.create_invitation.form.phone_placeholder')
                                           ])

            @include('forms.select', [
                'label' => 'Tipo',
                'field' => 'type',
                'options' => \App\User::dropdownOptions()
            ])


            @include('forms.submit-button', ['submitText' => __('copies.create_group.submit_button')])
        </form>



    @endcomponent

@endsection
