@extends('layouts.default')

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js" async></script>
@endpush

@section('content')
    @include('components.alert')
    <x-breadcrumbs :branch="\App\View\Components\Breadcrumbs::PATH_USERS" deep="indexUsers" />
    @component('components.card')
        @slot('title')
            <x-card-header>
                <x-slot name="leftButton">
                    <a href="{{ route('edit-user', [$user->id]) }}" class="btn">{{ __('copies.show_user.edit_link') }}</a>
                </x-slot>
                <x-slot name="title">
                    {{ __('copies.show_user.card_title') }}
                </x-slot>
            </x-card-header>
        @endslot




        <div>
            <div class="py-5 px-3 bg-gray-200 sm:flex">
                <label class="text-base leading-5 font-medium text-gray-700 w-1/3">{{ __('copies.show_user.full_name') }}</label>
                <p class="pl-1 flex-grow text-base leading-5 text-gray-900">{{ $user->name() }}</p>
            </div>

            <div class="py-5 px-3 sm:flex">
                <label class="text-base leading-5 font-medium text-gray-700 w-1/3">{{ __('copies.show_user.local_id_label') }}</label>
                <p class="pl-1 text-base leading-5 text-gray-900 flex-grow">{{ $user->local_id }}</p>
            </div>

            <div class="py-5 px-3 bg-gray-200 sm:flex">
                <label class="text-base leading-5 font-medium text-gray-700 w-1/3">{{ __('copies.show_user.email_label') }}</label>
                <p class="pl-1 flex-grow text-base leading-5 text-gray-900">{{ $user->email ?? __('copies.show_user.no_email_text') }}</p>
            </div>

            <div class="py-5 px-3 sm:flex">
                <label class="text-base leading-5 font-medium text-gray-700 w-1/3">{{ __('copies.show_user.phone_label') }}</label>
                <p class="pl-1 flex-grow text-base leading-5 text-gray-900">{{ $user->phone ?? __('copies.show_user.no_phone_text') }}</p>
            </div>

            <div class="py-5 px-3 sm:flex bg-gray-200">
                <label class="text-base leading-5 font-medium text-gray-700 w-1/3">{{ __('copies.show_user.type_label') }}</label>
                <p class="pl-1 flex-grow text-base leading-5 text-gray-900">{{ __('copies.user_types.' . $user->type) }}</p>
            </div>

            @can('viewLoginToken', $user)
                @if($user->login_token)
                    <div class="py-5 px-3 sm:flex">
                        <label class="text-base leading-5 font-medium text-gray-700 w-1/3">{{ __('copies.show_user.login_token_label') }}</label>
                        <p class="pl-1 flex-grow text-base leading-5 text-gray-900">
                            <span id="login-token-link" data-clipboard-target="#login-token-link" onclick="copyLoginLink()" class="link underline cursor-pointer">{{ route('login-token', $user->login_token) }}</span>
                        </p>
                    </div>
                @endif
            @endcan

        </div>

        <livewire:users.courses :user="$user" />
        <livewire:users.taken-courses :user="$user" />
        <livewire:users.dictated-courses :user="$user" />
    @endcomponent

    <script>
        function copyLoginLink() {
            var clipboard = new ClipboardJS('#login-token-link');
            clipboard.on('success', function(e) {
                clipboard.destroy();
            });
            alert('Copiado al portapapeles');
        }
    </script>

@endsection



