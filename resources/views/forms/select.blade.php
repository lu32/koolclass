<div class="flex flex-wrap {{ !$errors->has($field) ? 'mb-4' : 'mb-8' }}">
    <label for="name" class="field-label font-bold">{{ $label }}</label>
    <div class="field-container">
        <div class="relative">
            @include('components.form-alert-icon', ['field' => $field])
            <select name="{{ $field }}"
                      id="{{ $field }}"
                      class="field bg-white @error($field) field-error @enderror"
                      cols="30"
                      rows="4"
            >
{{--                {{ old($field) ?:( isset($value)? $value : '') }}--}}
                @foreach($options as $option)
                    <option value="{{ $option->id }}" {{ isset($selectedOption) ? ($selectedOption == $option->id ? 'selected' : '') : '' }}>{{ $option->name }}</option>
                @endforeach
            </select>
        </div>
        @error($field)
        <span class="field-error-message" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
