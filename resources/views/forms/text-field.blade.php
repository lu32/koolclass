<div class="flex flex-wrap items-center {{ !$errors->has($field) ? 'mb-4' : 'mb-8' }}">
    <label for="name" class="field-label font-bold">{{ $label }}</label>
    <div class="field-container">
        <div class="relative">
            @include('components.form-alert-icon', ['field' => $field])
            <input id="{{ $field }}" type="{{ $type ?? 'text' }}"
                   class="field {{ isset($readonly) ? 'bg-gray-300' : '' }}
                               @error($field) field-error @enderror"
                   @isset($placeholder)
                       placeholder="{{ $placeholder }}"
                   @endisset
                   name="{{ $field }}"
                   {{ isset($readonly) ? 'readonly' : '' }}

                   value="{{ old($field) ?:( isset($value)? $value : '') }}" {{ isset($required) ? 'requierd' : '' }}
                   autocomplete="off"
                   autofocus>
        </div>
        @error($field)
        <span class="field-error-message" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
