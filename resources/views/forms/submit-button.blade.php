<div class="flex flex-wrap mb-4">
    <label class="field-label"></label>
    <div class="field-container offset-md-4">
        <button type="submit" class="btn btn-primary">{{ $submitText }}</button>
    </div>
</div>
