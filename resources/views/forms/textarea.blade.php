<div class="flex flex-wrap {{ !$errors->has($field) ? 'mb-4' : 'mb-8' }}">
    <label for="name" class="field-label">{{ $label }}</label>
    <div class="field-container">
        <div class="relative">
            @include('components.form-alert-icon', ['field' => $field])
            <textarea style="height: initial" name="{{ $field }}"
                      id="{{ $field }}"
                      class="field @error($field) field-error @enderror"
                      cols="30"
                      rows="4"
            >{{ old($field) ?:( isset($value)? $value : '') }}</textarea>
        </div>
        @error($field)
        <span class="field-error-message" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
