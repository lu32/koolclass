<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- Styles -->
    {{--    <link href="{{ asset('build/app.css') }}" rel="stylesheet">--}}
    <link href="{{ mix('build/app.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/trix@1.2.3/dist/trix.css" rel="stylesheet">
    <link rel="manifest" href="/assets/manifest/manifest.json">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon-16x16.png">
    <meta name="theme-color" content="#ffffff">
    @livewireStyles
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

</head>
<body>

@include('layouts.partials.header')

<main class="py-4" id="page-content">
    @yield('content')
</main>

<!-- Scripts -->
<script src="{{ mix('build/app.js') }}" ></script>
@stack('scripts')
@livewireScripts
<script type="text/javascript" src="https://unpkg.com/trix@1.2.3/dist/trix.js"></script>
</body>
</html>
