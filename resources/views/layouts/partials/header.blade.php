<header class="flex justify-between items-center bg-gray-900  px-4 py-2">

    <div class="flex items-center justify-center sm:justify-start p-0 w-1/12 sm:w-4/12 md:w-2/12">
        <a href="{{ route('home') }}">
            <span class="text-white font-bold text-2xl">
                <span>K</span><span class="hidden sm:inline">oolclass</span>
            </span>
        </a>
    </div>

    @auth
    <div class="flex justify-center p-0">
        <a href="{{ route('index-courses') }}" class="block px-2 py-1 text-white font-semibold rounded hover:bg-gray-800">{{ __('copies.header.courses_link') }}</a>
        <a href="{{ route('index-groups') }}" class="block px-2 py-1 text-white font-semibold rounded hover:bg-gray-800 mt-0 ml-2">{{ __('copies.header.groups_link') }}</a>
    </div>
    @endauth

    <div class="flex justify-center sm:justify-end @auth w-1/12 sm:w-3/12 md:w-2/12 @endauth">
        @guest
            <a class="mt-1 block px-2 py-1 text-white font-semibold rounded hover:bg-gray-800 sm:mt-0 sm:ml-2" href="{{ route('login') }}">{{ __('copies.header.login_link') }}</a>
            <a class="mt-1 block px-2 py-1 text-white font-semibold rounded hover:bg-gray-800 sm:mt-0 sm:ml-2" href="{{ route('register') }}">{{ __('copies.header.register_link') }}</a>
        @else
            <div class="relative block">
                <button id="toggle-profile-dropdown" class="relative z-10 block h-8 w-8 rounded-full overflow-hidden border-2 border-gray-600 focus:outline-none focus:border-white">
                    <img class="h-full w-full object-cover" src="https://images.unsplash.com/photo-1487412720507-e7ab37603c6f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&q=80" alt="Your avatar">
                </button>
                <button id="profile-dropdown-shade" tabindex="-1" class="hidden fixed inset-0 h-full w-full bg-black opacity-50 cursor-default z-40"></button>
                <div id="profile-dropdown" class="hidden absolute right-0 mt-2 py-2 w-48 bg-white rounded-lg shadow-xl z-50">
                    <a href="{{ route('dashboard') }}" class="block px-4 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">{{ __('copies.header.dashboard_link') }} </a>
                    <a href="{{ route('show-user', [auth()->id()]) }}" class="block px-4 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white">{{ __('copies.header.profile_link') }}</a>
                    <a href="#" class="block px-4 py-2 text-gray-800 hover:bg-indigo-500 hover:text-white"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                    >{{ __('copies.header.logout_link') }}</a>
                </div>
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @endguest
    </div>
</header>
