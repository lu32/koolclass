export default {
    init() {
        console.log('init menu')
        const handleEscape = (e) => {
            if (e.key === 'Esc' || e.key === 'Escape') {
                toggleDropdown()
                document.removeEventListener('keydown', handleEscape)
            }
        }
        const toggleDropdown = () => {
            const profileDropdownShade = document.getElementById('profile-dropdown-shade')
            if (profileDropdownShade.classList.contains('hidden')) {
                document.addEventListener('keydown', handleEscape)
            } else {
                document.removeEventListener('keydown', handleEscape)
            }
            profileDropdownShade.classList.toggle('hidden')
            document.getElementById('profile-dropdown').classList.toggle('hidden')
        }

        let toggleProfileDropdown = document.getElementById('toggle-profile-dropdown')
        if (toggleProfileDropdown) {
            toggleProfileDropdown.addEventListener('click', toggleDropdown)
        }

        let profileDropdownShade = document.getElementById('profile-dropdown-shade')
        if (profileDropdownShade) {
            profileDropdownShade.addEventListener('click', toggleDropdown)
        }
    }
}
