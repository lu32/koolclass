<?php

use Illuminate\Support\Facades\Route;

// https://koolclass.com/call-for-code link del video

Auth::routes();


Route::get('/', 'HomeController@welcome')
     ->name('home');

Route::get('/dashboard', 'HomeController@dashboard')
     ->name('dashboard');


#Courses

Route::get('/courses', 'CourseController@index')
     ->name('index-courses');

Route::get('/courses/create', 'CourseController@create')
     ->name('create-course');

Route::post('/courses/create', 'CourseController@store')
    ->name('store-course');

Route::get('/courses/{course}', 'CourseController@show')
    ->name('show-course');

Route::get('/courses/{course}/edit', 'CourseController@edit')
     ->name('edit-course');

//Route::get('/courses/{course}/folders/{folder}', 'CourseController@files')
//     ->name('course-files');

Route::livewire('/courses/{course}/folders/{folder}', 'courses.files')
    ->name('course-files')->layout('layouts.default');

Route::patch('/courses/{course}', 'CourseController@update')
    ->name('update-course');

Route::livewire('/courses/{course}/messages', 'courses.messages')
    ->name('course-messages')->layout('layouts.default');

Route::livewire('/courses/{course}/messages/create', 'courses.messages.create')
    ->name('create-message')->layout('layouts.default');


#Groups

Route::get('/groups', 'GroupController@index')
    ->name('index-groups');

Route::get('/groups/create', 'GroupController@create')
    ->name('create-group');

Route::post('/groups/create', 'GroupController@store')
    ->name('store-group');

Route::patch('/groups/{group}', 'GroupController@update')
    ->name('update-group');

Route::get('/groups/{group}', 'GroupController@show')
    ->name('show-group');

Route::get('/groups/{group}/edit', 'GroupController@edit')
    ->name('edit-group');

Route::get('/groups/{group}/courses', 'GroupCoursesController@courses')
    ->name('group-courses');

Route::get('/groups/{group}/add-subject', 'GroupCoursesController@addSubjectForm')
    ->name('add-subject');

Route::post('/groups/{group}/courses', 'GroupCoursesController@addCourse');

Route::get('/groups/{group}/courses/{course}', 'GroupCoursesController@course')
    ->name('show-course-in-group');

Route::get('/groups/{group}/students', 'GroupStudentsController@students')
    ->name('group-students')->middleware('auth');

Route::post('/groups/{group}/students', 'GroupStudentsController@addStudent')
    ->name('add-student-to-group')->middleware('auth');

Route::get('/web-api/search-users', 'SearchUsersController@search')
    ->name('search-users')->middleware('auth');

#files

Route::post('web-api/signed-url', 'CloudDiskController@signedUrl')
     ->name('signed-url');

Route::post('web-api/store-file', 'UploadsController@store')
    ->name('store-file');


#users

Route::get('/users', 'UsersController@index')->name('index-users');
Route::get('/users/invite', 'UsersController@create')->name('create-user');
Route::post('/users', 'UsersController@store')->name('store-user');
Route::get('/users/{user}', 'UsersController@show')->name('show-user');
Route::get('/users/{user}/edit', 'UsersController@edit')->name('edit-user');
Route::patch('/users/{user}', 'UsersController@update')->name('update-user');
Route::get('/login-token/{token}', 'LoginTokenController@welcome')->name('login-token');
Route::post('/login-token/{token}', 'LoginTokenController@updateAndLogin');

#Folders

Route::post('/folders/{parentFolder}', 'FolderController@store')
    ->name('store-folder');
