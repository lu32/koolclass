module.exports = {
  theme: {
    extend: {
        boxShadow: {
            "error": '0 0 0 3px rgba(229,62,62, 0.5)',
        },
        minWidth: {
            'initial': 'initial',
            '0': '0',
            '1/4': '25%',
            '1/2': '50%',
            '3/4': '75%',
            'full': '100%',
        },
        backgroundColor: theme => ({
            'shade': 'rgba(0, 0,  0, 0.5)'
        })
    },


  },
  variants: {},
  plugins: []
}
