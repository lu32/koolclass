<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\User;

class Institution extends Model
{
    protected $fillable = ['name'];

    public function teachers()
    {
        return $this->hasMany(User::class, 'institution_id')
                    ->where('type', User::TEACHER_TYPE);
    }

    public function groups()
    {
        return $this->hasMany(Group::class, 'institution_id');
    }
    
    public function users()
    {
        return $this->hasMany(User::class);
    }
    
}
