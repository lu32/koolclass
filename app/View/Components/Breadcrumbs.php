<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Breadcrumbs extends Component
{
    const PATH_GROUP_COURSES  = 'groups_courses';
    const PATH_USERS  = 'users';
    const PATH_GROUP_STUDENTS  = 'group_students';
    const PATH_COURSES  = 'courses';

    public $branch, $deep, $course, $group, $links = [], $user;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($deep,
                                $branch,
                                $course = null,
                                $group = null, $user = null)
    {
        $this->course = $course;
        $this->group = $group;
        $this->deep = $deep;
        $this->branch = $branch;
        $this->user = $user;
        $this->build();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.breadcrumbs');
    }

    public function build()
    {
        $crumbs = [
            self::PATH_COURSES => [
                'dashboard',
                'indexCourses',
                'showCourse',
                'courseMessages',
            ],
            'groups' => [
                'dashboard',
                'indexGroups',
                'showGroup',
                'addGroupSubjects',
            ],
            self::PATH_GROUP_COURSES => [
                'dashboard',
                'indexGroups',
                'showGroup',
                'addGroupSubjects',
            ],
            self::PATH_GROUP_STUDENTS => [
                'dashboard',
                'indexGroups',
                'showGroup',
            ],
            self::PATH_USERS => [
                'dashboard',
                'indexUsers',
                'showUser',
            ]
        ];

        foreach ($crumbs[$this->branch] as $crumb) {
            $this->$crumb();
            if ($crumb == $this->deep) {
               break;
            }
        }
    }

    private function dashboard()
    {
        $this->links[] = [
            'url' => route('dashboard'),
            'text' => __('copies.breadcrumbs.dashboard'),
        ];
    }

    private function indexCourses()
    {
        $this->links[] = [
            'url' => route('index-courses'),
            'text' => __('copies.breadcrumbs.index_courses'),
        ];
    }

    private function showCourse()
    {
        $this->links[] = [
            'url' => route('show-course', [$this->course->id]),
            'text' => $this->course->name,
        ];
    }

    private function indexGroups()
    {
        $this->links[] = [
            'url' => route('index-groups'),
            'text' => __('copies.breadcrumbs.index_groups'),
        ];
    }

    private function showGroup()
    {
        $this->links[] = [
            'url' => route('show-group', [$this->group->id]),
            'text' => $this->group->name,
        ];
    }

    private function addGroupSubjects()
    {
        $this->links[] = [
            'url' => route('group-courses', [$this->group->id]),
            'text' => __('copies.breadcrumbs.group_subjects'),
        ];
    }

    private function indexUsers()
    {
        $this->links[] = [
            'url' => route('index-users'),
            'text' => __('copies.breadcrumbs.index_users'),
        ];
    }

    private function showUser()
    {
        $this->links[] = [
            'url' => route('show-user', [$this->user->id]),
            'text' => $this->user->first_name,
        ];
    }

    private function courseMessages()
    {
        $this->links[] = [
            'url' => route('course-messages', [$this->course->id]),
            'text' => __('copies.breadcrumbs.course_messages'),
        ];
    }


}

