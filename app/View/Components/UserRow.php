<?php

namespace App\View\Components;

use Illuminate\View\Component;

class UserRow extends Component
{

    public $user, $borderB;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($user, $borderB)
    {
        $this->user = $user;
        $this->borderB = $borderB;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.user-row');
    }
}
