<?php

namespace App;

use App\Filters\FrameFilters;
use App\Filters\UsersFilter;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    const STUDENT_TYPE = 'student';
    const TEACHER_TYPE = 'teacher';
    const COORDINATOR_TYPE = 'coordinator';
    const ADMIN_TYPE = 'admin';
    const PARENT_TYPE = 'parents';

    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        return $this->type === self::ADMIN_TYPE;
    }


    public static function types()
    {
        return [self::STUDENT_TYPE, self::TEACHER_TYPE, self::ADMIN_TYPE, self::COORDINATOR_TYPE];
    }

    public static function dropdownOptions()
    {
        $options = [];
        $options[] = (object) ['id' => '', 'name' => __("copies.user_types.first_option")];
        foreach (self::types() as $type) {
            $options[] = (object) ['id' => $type, 'name' => __("copies.user_types.{$type}")];
        }

        return $options;
    }

    public function courses(){
        return $this->hasMany(Course::class, 'creator_id');
    }

    public function createdGroups() {
        return $this->hasMany(Group::class, 'creator_id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }


    public function institution() {
        return $this->belongsTo(Institution::class, 'institution_id');
    }

    public function name()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Apply all relevant frame filters.
     *
     * @param  Builder       $query
     * @param  FrameFilters $filters
     * @return Builder
     */
    public function scopeFilter($query)
    {
        $filters = app(UsersFilter::class);
        return $filters->apply($query);
    }

    public function canTeach()
    {
        return $this->type != self::STUDENT_TYPE;
    }

}
