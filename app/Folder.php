<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $guarded = [];

    public function uploads()
    {
        return $this->hasMany(Upload::class);
    }

    public function folders()
    {
        return $this->hasMany(Folder::class, 'parent_id');
    }
}
