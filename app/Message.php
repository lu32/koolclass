<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Purify;

class Message extends Model
{
    protected $guarded = [];

    public function getBodyAttribute($body)
    {
        return Purify::clean($body);
    }

    public function messageable()
    {
        return $this->morphTo();
    }

}
