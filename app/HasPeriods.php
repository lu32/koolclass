<?php


namespace App;


trait HasPeriods
{
    public function periodLabel()
    {
        return PeriodsCollection::find($this->period)->name;
    }

}
