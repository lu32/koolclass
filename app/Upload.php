<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    public function folder()
    {
        return $this->belongsTo(Folder::class);
    }

    public function isImage()
    {
        return in_array($this->extension, ['jpg', 'jpeg', 'png', 'apng', 'bmp', 'jfif', 'jfif', 'pjp', 'svg', 'tif', 'tiff', 'webp', 'gif',]);
    }

}
