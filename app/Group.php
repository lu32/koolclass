<?php

namespace App;

use App\Filters\GroupsFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasPeriods;

    protected $guarded = [];

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function institution()
    {
        return $this->belongsTo(Institution::class, 'institution_id');
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class);
    }

    public function students()
    {
        return $this->belongsToMany(User::class)
            ->where('type', User::STUDENT_TYPE);
    }

    /**
     * Apply all relevant frame filters.
     *
     * @param  Builder       $query
     * @param  FrameFilters $filters
     * @return Builder
     */
    public function scopeFilter($query)
    {
        $filters = app(GroupsFilter::class);
        return $filters->apply($query);
    }


}

