<?php

namespace App;

use App\Filters\CoursesFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasPeriods;

    protected $fillable = [
        'name',
        'creator_id',
        'institution_id',
    ];

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function mainFolder()
    {
        return $this->hasOne(Folder::class)->whereNull('parent_id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class)->orderBy('name');
    }

    public function getMainFolder()
    {
        if($this->mainFolder) return $this->mainFolder;

        return Folder::create(['course_id' => $this->id, 'name' => 'root']);
    }

    /**
     * Apply all relevant frame filters.
     *
     * @param  Builder       $query
     * @param  FrameFilters $filters
     * @return Builder
     */
    public function scopeFilter($query)
    {
        $filters = app(CoursesFilter::class);
        return $filters->apply($query);
    }


    public function messages()
    {
        return $this->morphMany(Message::class, 'messageable');
    }



}
