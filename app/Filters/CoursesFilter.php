<?php

namespace App\Filters;

class CoursesFilter extends Filters
{
    protected $filters = [
        'year',
        'period',
        'creator_id',
    ];

    public function year($year)
    {
        $this->builder->where('year', $year);
    }

    public function period($period)
    {
        $this->builder->where('period', $period);
    }

    public function creator_id($creator_id)
    {
        $this->builder->where('creator_id', $creator_id);
    }


}
