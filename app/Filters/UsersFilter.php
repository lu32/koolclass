<?php

namespace App\Filters;

class  UsersFilter extends Filters
{
    protected $filters = [
        'first_name',
        'last_name',
        'local_id',
        'type',
    ];

    public function first_name($first_name)
    {
        $this->builder->whereRaw("LOWER(first_name) LIKE LOWER(?)", "%{$first_name}%");
    }

    public function last_name($last_name)
    {
        $this->builder->whereRaw("LOWER(last_name) LIKE LOWER(?)", "%{$last_name}%");
    }

    public function local_id($local_id)
    {
        $this->builder->whereRaw("LOWER(local_id) LIKE LOWER(?)", "%{$local_id}%");
    }

    public function type($type)
    {
        $this->builder->where('type', $type);
    }

}
