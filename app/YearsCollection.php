<?php

namespace App;

class YearsCollection
{
    public static function get()
    {
        return [
            (object)['id' => '', 'name' => 'Seleccionar'],
            (object)['id' => 2020, 'name' => 2020],
            (object)['id' => 2021, 'name' => 2021],
            (object)['id' => 2022, 'name' => 2022],
            (object)['id' => 2023, 'name' => 2023],
            (object)['id' => 2024, 'name' => 2024],
            (object)['id' => 2025, 'name' => 2025]
        ];
    }
}
