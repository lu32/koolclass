<?php

namespace App;

class PeriodsCollection
{
    const PERIOD_ALL_YEAR = 0;
    const PERIOD_FIRST_SEMESTER = 1;
    const PERIOD_SECOND_SEMESTER = 2;
    const PERIOD_INTERSEMESTER = 3;

    public static function all()
    {
        return collect(self::get())->pluck('id')->all();
    }

    public static function get()
    {
        return [
            (object)['id' => '', 'name' => __('copies.period.list.select')],
            (object)['id' => self::PERIOD_ALL_YEAR, 'name' => __('copies.period.list.0')],
            (object)['id' => self::PERIOD_FIRST_SEMESTER, 'name' => __('copies.period.list.1')],
            (object)['id' => self::PERIOD_SECOND_SEMESTER, 'name' => __('copies.period.list.2')],
            (object)['id' => self::PERIOD_INTERSEMESTER, 'name' => __('copies.period.list.3')]
        ];
    }

    public static function find($id)
    {
        return collect(self::get())->firstWhere('id', '===', $id);
    }

}
