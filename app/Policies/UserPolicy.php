<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        return $user->institution_id == $model->institution_id;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function viewLoginToken(User $user, User $model)
    {
        return in_array($user->type, [User::TEACHER_TYPE, User::ADMIN_TYPE, User::COORDINATOR_TYPE]);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->type, [User::TEACHER_TYPE, User::ADMIN_TYPE]);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        return (
                    in_array($user->type, [User::TEACHER_TYPE, User::ADMIN_TYPE, User::COORDINATOR_TYPE])
                    &&
                    $user->institution_id == $model->institution_id
               )
               || $user->id == $model->id;
    }

    public function addUserToGroup(User $user, User $model)
    {
        return  in_array($user->type, [User::TEACHER_TYPE, User::ADMIN_TYPE, User::COORDINATOR_TYPE])
                &&
                $user->institution_id == $model->institution_id;
    }



}
