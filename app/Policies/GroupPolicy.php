<?php

namespace App\Policies;

use App\Group;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the group
     *
     * @param  \App\User  $user
     * @param  \App\Course  $course
     * @return mixed
     */
    public function view(User $user, Group $group)
    {
        return $user->institution_id == $group->institution_id;
    }

    /**
     * Determine whether the user can update the group,
     *
     * @param  \App\User  $user
     * @param  \App\Course  $course
     * @return mixed
     */
    public function update(User $user, Group $group)
    {
        return $user->institution == $group->institution
            and in_array($user->type, [User::ADMIN_TYPE, User::COORDINATOR_TYPE, User::TEACHER_TYPE]);
    }

    public function addStudent(User $user, Group $group)
    {
        return in_array($user->type, [User::TEACHER_TYPE, User::COORDINATOR_TYPE, User::ADMIN_TYPE])
              && $user->institution_id == $group->institution_id;
    }

    /**
     * Determine whether the user can update the group,
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->type != User::STUDENT_TYPE;
    }



}
