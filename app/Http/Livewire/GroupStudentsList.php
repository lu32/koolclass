<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Collection;

class GroupStudentsList extends Component
{
    public $group;
    /**
     * @var Collection
     */
    public $students;

    protected $listeners = [
        'userDeleted'
    ];


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function mount($group)
    {
        $this->group = $group;
        $this->students = $group->students;
    }


    public function render()
    {
        return view('livewire.group-students-list');
    }

    public function userDeleted($id)
    {
        $this->students = $this->students->filter(function($student)use($id){
            return $student->id != $id;
        });
    }

}
