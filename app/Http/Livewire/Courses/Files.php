<?php

namespace App\Http\Livewire\Courses;

use App\Course;
use App\Folder;
use Livewire\Component;

class Files extends Component
{
    public $course, $folder;

    public function mount(Course $course, Folder $folder)
    {
        $this->course = $course;
        $this->folder = $folder;
    }

    public function render()
    {
        return view('livewire.courses.files');
    }
}
