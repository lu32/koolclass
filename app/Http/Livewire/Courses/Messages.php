<?php

namespace App\Http\Livewire\Courses;

use Livewire\Component;
use App\Course;

class Messages extends Component
{
    public $course;

    public function mount(Course $course)
    {
        $this->course = $course;
    }

    public function render()
    {
        return view('livewire.courses.messages');
    }
}
