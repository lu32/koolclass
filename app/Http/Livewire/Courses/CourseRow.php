<?php

namespace App\Http\Livewire\Courses;

use Livewire\Component;

class CourseRow extends Component
{
    public $course;
    public $isLast;

    public function mount($course, $isLast)
    {
        $this->course = $course;
        $this->isLast = $isLast;
    }


    public function render()
    {
        return view('livewire.courses.course-row');
    }
}
