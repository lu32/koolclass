<?php

namespace App\Http\Livewire\Courses\Messages;

use App\Course;
use App\Message;
use Livewire\Component;

class Create extends Component
{
    public $course;
    public $body;
    public $title;
    public $message;

    public function mount(Course $course)
    {
        $this->course = $course;
        $this->title = '';
        $this->body = '';
    }

    public function render()
    {
        return view('livewire.courses.messages.create');
    }

    public function save()
    {
        if ($this->message) {

            return redirect()->route('course-messages', $this->course->id);
        }
        $message = Message::make(['title' => $this->title, 'body' => $this->body]);
        $this->course->messages()->save($message);
        $this->message = $message;

        return redirect()->route('course-messages', $this->course->id);

    }

}
