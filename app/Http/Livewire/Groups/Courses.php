<?php

namespace App\Http\Livewire\Groups;

use Livewire\Component;

class Courses extends Component
{
    public $group;

    public function mount($group)
    {
        $this->group = $group;
    }

    public function render()
    {
        return view('livewire.groups.courses');
    }
}
