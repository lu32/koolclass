<?php

namespace App\Http\Livewire\Groups;

use App\Course;
use App\Group;
use Livewire\Component;

class GroupsIndex extends Component
{
    public $groups;
    public $teachers = [];
    public $year = '';
    public $period = '';
    public $creator_id = '';
    protected $updatesQueryString = [
        'year' => ['except' => ''],
        'period' => ['except' => ''],
        'creator_id' => ['except' => '']
    ];


    public function mount()
    {
        $this->teachers = auth()->user()->institution->teachers;
        $this->fill([
            'year' => request()->query('year', $this->year),
            'period' => request()->query('period', $this->period),
            'creator_id' => request()->query('creator_id', $this->creator_id),
        ]);
        $this->filter();
    }


    public function render()
    {
        return view('livewire.groups.groups-index');
    }

    public function filter()
    {
        request()->merge(['period' => $this->period, 'year' => $this->year, 'creator_id' => $this->creator_id]);
        $this->groups = Group::filter()
            ->where('institution_id', auth()->user()->institution_id)
            ->get();
    }

    public function getAreFiltersEmptyProperty()
    {
        return !$this->year && !$this->period && !$this->creator_id;
    }


}
