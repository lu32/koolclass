<?php

namespace App\Http\Livewire\Groups;

use Livewire\Component;

class GroupRow extends Component
{
    public $group, $isLast;

    public function mount($group, $isLast)
    {
        $this->group = $group;
        $this->isLast = $isLast;
    }


    public function render()
    {
        return view('livewire.groups.group-row');
    }
}
