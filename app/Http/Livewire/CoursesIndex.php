<?php

namespace App\Http\Livewire;

use App\Course;
use App\Filters\CoursesFilter;
use Livewire\Component;

class CoursesIndex extends Component
{

    protected $updatesQueryString = ['year' => ['except' => ''], 'period' => ['except' => ''], 'creator_id' => ['except' => '']];


    public $courses;
    public $year = '';
    public $period = '';
    public $creator_id = '';
    public $teachers = [];


    public function mount()
    {
        $this->year = request()->query('year', $this->year);
        $this->period = request()->query('period', $this->period);
        $this->creator_id = request()->query('creator_id', $this->creator_id);
        $this->filter();
        $this->teachers = auth()->user()->institution->teachers;
    }

    public function render()
    {
        return view('livewire.courses.courses-index');
    }

    public function filter()
    {
        request()->merge(['period' => $this->period, 'year' => $this->year, 'creator_id' => $this->creator_id]);
        $this->courses = Course::filter()
                               ->where('institution_id', auth()->user()->institution_id)
                               ->get();

    }

    public function getAreFiltersEmptyProperty()
    {
        return !$this->year && !$this->period && !$this->creator_id;
    }

}
