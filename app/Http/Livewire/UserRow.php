<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class UserRow extends Component
{
    use AuthorizesRequests;

    public $user;
    public $borderB;
    public $showRemoveWarning;
    public $group;
    public $removed = false;
    public $iteration;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function mount($user, $group = null, $borderB, $iteration)
    {
        $this->user = $user;
        $this->borderB = $borderB;
        $this->showRemoveWarning = false;
        $this->group = $group;
        $this->iteration = $iteration;
    }

    public function render()
    {
        return view('livewire.user-row');
    }

    public function showRemoveWarning()
    {
        $this->showRemoveWarning = true;
    }

    public function hideRemoveWarning()
    {
        $this->showRemoveWarning = false;
    }

    public function removeStudent()
    {
//        $this->authorize();
        $this->group->students()->detach($this->user->id);
        $this->removed = true;
        $this->emitUp('userDeleted', $this->user->id);

    }



}
