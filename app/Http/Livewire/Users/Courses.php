<?php

namespace App\Http\Livewire\Users;

use App\User;
use Livewire\Component;

class Courses extends Component
{
    public $user;

    public function mount(User $user)
    {
        $this->user = $user;
    }

    public function render()
    {
        return view('livewire.users.courses');
    }
}
