<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;

class GroupsTable extends Component
{
    public $groups;
    
    public function mount($groups)
    {
        $this->groups = $groups;
    }
    
    public function render()
    {
        return view('livewire.users.groups-table');
    }
}
