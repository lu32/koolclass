<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;

class DictatedCourses extends Component
{
    public $user;

    public function mount($user)
    {
        $this->user = $user;
    }

    public function render()
    {
        return view('livewire.users.dictated-courses');
    }
}
