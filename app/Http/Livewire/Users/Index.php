<?php

namespace App\Http\Livewire\Users;

use App\User;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $first_name;
    public $last_name;
    public $local_id;

    protected $updatesQueryString = [
        'first_name' => ['except' => ''],
        'last_name' => ['except' => ''],
        'local_id' => ['except' => ''],
    ];

    public function mount()
    {
        $this->first_name = request()->query('first_name', $this->first_name);
        $this->last_name = request()->query('last_name', $this->last_name);
    }

    public function updatedFirstName()
    {
        request()->merge(['first_name' => $this->first_name]);
        $this->resetPage();

    }

    public function updatedLastName()
    {
        request()->merge(['last_name' => $this->last_name]);
        $this->resetPage();

    }

    public function updatedLocalId()
    {
        request()->merge(['local_id' => $this->local_id]);
        $this->resetPage();

    }


    public function render()
    {
        return view('livewire.users.index', [
            'users' => User::filter()
                           ->where('institution_id', auth()->user()->institution_id)
                           ->paginate(30),
            'nombre' => $this->first_name
        ]);
    }
}
