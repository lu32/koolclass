<?php

namespace App\Http\Controllers;

use App\Course;
use App\Folder;
use App\PeriodsCollection;
use App\YearsCollection;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('courses.index');
    }

    public function create()
    {
        $periods = PeriodsCollection::get();
        $years = YearsCollection::get();

        return view('courses.create', ['years' => $years, 'periods' => $periods]);
    }

    public function store()
    {
        $this->authorize('create', Course::class);
        $course = new Course;
        $course->name = request('name');
        $course->year = request('year');
        $course->period = request('period');
        $course->creator_id = auth()->id();
        $course->institution_id = auth()->user()->institution_id;
        $course->save();

        $folder = new Folder;
        $folder->name='Main Folder';
        $folder->course_id = $course->id;
        $folder->save();

        return redirect()->route('show-course', [$course->id])
                         ->with('success', __('copies.store_course.success_message'));
    }

    public function show(Course $course)
    {
        $this->authorize('view', $course);

        return view('courses.show', compact('course'));
    }

    public function edit(Course $course)
    {
        $this->authorize('view', $course);

        return view('courses.edit', compact('course'));
    }

    public function update(Course $course)
    {
        $this->authorize('view', $course);
        $course->name = request('name');
        $course->save();

        return redirect(route('show-course', [$course->id]))->with('success', __('copies.update_course.success_message'));
    }

    public function files(Course $course, Folder $folder)
    {
        $this->authorize('view', $folder);

        return view('courses.files', compact('course', 'folder'));
    }
}
