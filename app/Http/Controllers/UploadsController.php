<?php

namespace App\Http\Controllers;

use App\Folder;
use App\Upload;
use Illuminate\Http\Request;
use Storage;

class UploadsController extends Controller
{
    public function store()
    {
        $folder = Folder::findOrFail(request('folder_id'));
        $this->authorize('storeFiles', $folder);
        $this->saveUpload($folder);

        return response()->json(['status' => 'ok'], 201);
    }

    /**
     * @param $folder
     * @return array
     */
    private function saveUpload($folder): void
    {
        $name = basename(request('filename'));
        $explodedName = explode('.', $name);
        $extension = array_pop($explodedName);
        $key = request('key');
        $uuid = basename($key);
        $path = "uploads/{$uuid}.{$extension}";
        Storage::disk('s3')->move($key, $path);
        $size = Storage::disk('s3')->size($path);
        $upload = new Upload;
        $upload->path = $path;
        $upload->extension = $extension;
        $upload->name = $name;
        $upload->size = $size;
        $upload->folder_id = $folder->id;
        $upload->save();
    }

}
