<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $this->authorize('create', auth()->user());

        return view('users.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email',
            'phone' => 'numeric',
            'local_id' => ['required', 'numeric'],
            'type' => ['required', 'in:' . implode(',', User::types()),
                function ($attribute, $value, $fail) {
                    if ($value !== User::STUDENT_TYPE && !auth()->user()->isAdmin()) {
                        session()->flash('error', __('copies.create_invitation.validation_errors.teacher_can_only_create_students'));
                        $fail($attribute. " {$value} is invalid.");
                    }
                }
                ],
        ]);

        $newUser = User::create(array_merge($validatedData, ['institution_id' => auth()->user()->institution_id, 'login_token' => \Str::random(20) ]));

        return redirect()->route('show-user', [$newUser->id])->with('success', __('copies.create_invitation.succes_message'));
    }

    public function index()
    {
        $users = User::where('institution_id', auth()->user()->institution_id)->orderBy('last_name', 'asc')->get();

        return view('users.index', compact('users'));
    }

    public function show(User $user)
    {
        $this->authorize('view', $user);

        return view('users.show', compact('user'));
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user);

        return view('users.edit', compact('user'));
    }

    public function update(User $user)
    {
        if( !request()->user()->can('update', $user) ) {

            return redirect()->route('show-user', [$user->id])->with('error', __('copies.update_user.permission_error'));
        }

        $validatedData = $this->validate(request(), [
            'first_name' => 'string',
            'last_name' => 'string',
            'email' => 'email|nullable',
            'phone' => ['numeric', 'nullable'],
            'local_id' => [Rule::unique('users')->ignore($user)],
            'type' => ['in:' . implode(',', User::types()),
                function ($attribute, $value, $fail) {
                    if ($value !== User::STUDENT_TYPE && !auth()->user()->isAdmin()) {
                        session()->flash('error', __('copies.create_invitation.validation_errors.teacher_can_only_create_students'));

                        $fail("El " . trans('validation.attributes.type'). " solo puede ser asignado por un administrador.");
                    }
                }
            ],
        ]);
        $user->update($validatedData);

        return redirect()->route('show-user', [$user->id])->with('success', __('copies.update_user.success_message'));
    }






}
