<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use Hash;

class LoginTokenController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'welcome']);
    }

    public function welcome($token)
    {
        $user = User::where('login_token', $token)->first();

        return view('users.login-token', compact('user'));
    }

    public function updateAndLogin($token)
    {
        $this->validate(request(), [
            'password' => ['required', 'string', 'min:6', 'confirmed']
        ]);
        $user = User::where('login_token', $token)->first();
        $user->password = Hash::make(request('password'));
        $user->save();
        Auth::login($user);

        return redirect()->route('dashboard', ['message' => 'si']);
    }

}
