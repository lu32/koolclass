<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Str;

class CloudDiskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function signedUrl()
    {
        $s3 = Storage::disk('s3');
        $client = $s3->getDriver()->getAdapter()->getClient();
        $expiry = "+60 minutes";
        $uuid = Str::uuid();
        $key = "tmp/{$uuid}";

        $command = $client->getCommand('PutObject', [
            'Bucket' => config('filesystems.disks.s3.bucket'),
            'Key'    => $key
        ]);

        $request = $client->createPresignedRequest($command, $expiry);

        $signedUrl =  (string) $request->getUri();

        return [
            'signed_url' => $signedUrl,
            'key' => $key,
        ];
    }

}
