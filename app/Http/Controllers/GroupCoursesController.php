<?php

namespace App\Http\Controllers;

use App\Course;
use App\Group;
use Illuminate\Http\Request;

class GroupCoursesController extends Controller
{
    public function courses(Group $group)
    {
        return view('groups.courses', compact('group'));
    }

    public function addCourse(Group $group)
    {
        $this->validate(request(), [
            'course_id' => 'required|exists:courses,id'
        ]);
        $course = Course::findOrFail(request('course_id'));
        $this->authorize('addToGroup', $course);
        $group->courses()->syncWithoutDetaching([$course->id]);

        return redirect()->route('show-group', [$group->id])
            ->with('success', __('copies.group_courses.course_added_message') . " $group->name");
    }

    public function addSubjectForm(Group $group)
    {
        $availableCourses = auth()->user()->courses()
            ->where('year', $group->year)
            ->where('period', $group->period)->get();

        return view('groups.add-course', compact('group', 'availableCourses'));
    }

    public function course(Group $group, Course $course)
    {
        $this->authorize('view', $course);

        return view('courses.show', compact('course', 'group'));
    }




}
