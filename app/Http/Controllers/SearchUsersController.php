<?php

namespace App\Http\Controllers;

use App\Filters\UsersFilter;
use App\Http\Resources\UserCollection;
use App\User;
use Illuminate\Http\Request;

class SearchUsersController extends Controller
{
    const FILTERS = ['first_name', 'last_name', 'local_id', 'type'];

    public function search(UsersFilter $usersFilter)
    {
        $results = User::where('institution_id', auth()->user()->institution_id)->filter($usersFilter)->paginate(10)
                      ->appends(request()->only(self::FILTERS));

        return new UserCollection($results);
    }

}
