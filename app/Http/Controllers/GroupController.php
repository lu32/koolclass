<?php

namespace App\Http\Controllers;
use \App\Group;
use App\PeriodsCollection;
use App\YearsCollection;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Group $group)
    {
        return view('groups.show', compact('group'));
    }

    public function index()
    {
        $groups = Group::where('institution_id', auth()->user()->institution_id)->orderBy('name')->get();

        return view('groups.index', compact('groups'));
    }

    public function create()
    {
        $this->authorize('create', Group::class);
        $periods = PeriodsCollection::get();
        $years = YearsCollection::get();

        return view('groups.create', ['years' => $years, 'periods' => $periods]);
    }

    public function store()
    {
        $this->authorize('create', Group::class);
        $group = new Group;
        $group->name = request('name');
        $group->year = request('year');
        $group->period = request('period');
        $group->creator_id = auth()->id();
        $group->institution_id = auth()->user()->institution_id;
        $group->save();

        return redirect()->route('show-group', [$group->id])
            ->with('success', __('copies.store_group.success_message'));
    }

    public function edit(Group $group)
    {
        $this->authorize('update', $group);

        return view('groups.edit', compact('group'));
    }

    public function update(Group $group)
    {
        $this->authorize('update', $group);
        $validatedData = $this->validate(request(), [
            'name' => 'string',
            'year' => 'string',
            'period' => 'string',
        ]);
        $group->update($validatedData);
        $group->save();

        return redirect(route('show-group', [$group->id]))->with('success', __('copies.update_group.success_message'));
    }
}

