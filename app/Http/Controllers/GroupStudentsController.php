<?php

namespace App\Http\Controllers;

use App\Group;
use App\User;
use Illuminate\Http\Request;

class GroupStudentsController extends Controller
{
    public function students(Group $group)
    {
        $this->authorize('view', $group);

        return view('groups.students', compact('group'));
    }

    public function addStudent(Group $group)
    {
        $student = User::findOrFail(request('user_id'));
        if (!auth()->user()->can('add-student', $group)
            || !auth()->user()->can('add-user-to-group', $student)) {

            return redirect()->route('show-group', $group->id)
                ->with('error', __('copies.group_students.cant_add_to_group'));
        }
        $group->students()->syncWithoutDetaching([request('user_id')]);

        return redirect()->route('show-group', $group->id)->with('success', 'Estudiante agregado');
    }


}
