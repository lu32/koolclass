<?php

namespace App\Http\Controllers;

use App\Folder;
use App\Course;
use Illuminate\Http\Request;

class FolderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Folder $parentFolder) 
    {
        $this->authorize('storeFiles', $parentFolder);
        $folder = new Folder;
        $folder->name = request('name');
        $folder->parent_id = $parentFolder->id;
        $folder->course_id = $parentFolder->course_id;
        $folder->save();

        return redirect()->route('course-files', [$folder->course_id, $parentFolder->id])
                         ->with('success', __('copies.store_folder.success_message'));
    }
}
